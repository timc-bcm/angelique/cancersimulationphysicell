// Annotated example from POV-Ray tutorial
// http://www.povray.org/documentation/3.7.0/t2_2.html#t2_2_1_6
//
// Generate PNG with:
//
//   $ povray +IHelloSphere.pov
//

#include "colors.inc"  // Include color name macros

background { color Cyan }

// Lights!
//
light_source {
  <2, 4, -3>           // X, Y, and Z coordinates of the light source
  color White
}

// Camera!
//
camera {
  location <0, 2, -3>  // X, Y and Z coordinates of the camera
  look_at  <0, 1,  2>  // X, Y and Z coordinates of where the camera is aimed
}

// Object!
//
  union{
sphere {
  <0, 1, 2>, 2         // X, Y, Z and radius of the sphere
  texture {
    pigment { color Yellow }
  }
}
sphere {
  <2, 3, 4>, 2         // X, Y, Z and radius of the sphere
  texture {
    pigment { color Yellow }
  }
}

  }