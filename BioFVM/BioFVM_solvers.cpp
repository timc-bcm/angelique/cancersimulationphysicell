/*
#############################################################################
# If you use BioFVM in your project, please cite BioFVM and the version     #
# number, such as below:                                                    #
#                                                                           #
# We solved the diffusion equations using BioFVM (Version 1.1.7) [1]        #
#                                                                           #
# [1] A. Ghaffarizadeh, S.H. Friedman, and P. Macklin, BioFVM: an efficient #
#    parallelized diffusive transport solver for 3-D biological simulations,#
#    Bioinformatics 32(8): 1256-8, 2016. DOI: 10.1093/bioinformatics/btv730 #
#                                                                           #
#############################################################################
#                                                                           #
# BSD 3-Clause License (see https://opensource.org/licenses/BSD-3-Clause)   #
#                                                                           #
# Copyright (c) 2015-2017, Paul Macklin and the BioFVM Project              #
# All rights reserved.                                                      #
#                                                                           #
# Redistribution and use in source and binary forms, with or without        #
# modification, are permitted provided that the following conditions are    #
# met:                                                                      #
#                                                                           #
# 1. Redistributions of source code must retain the above copyright notice, #
# this list of conditions and the following disclaimer.                     #
#                                                                           #
# 2. Redistributions in binary form must reproduce the above copyright      #
# notice, this list of conditions and the following disclaimer in the       #
# documentation and/or other materials provided with the distribution.      #
#                                                                           #
# 3. Neither the name of the copyright holder nor the names of its          #
# contributors may be used to endorse or promote products derived from this #
# software without specific prior written permission.                       #
#                                                                           #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       #
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED #
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           #
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER #
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  #
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       #
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        #
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    #
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      #
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        #
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              #
#                                                                           #
#############################################################################
*/

#include "BioFVM_solvers.h"
#include "BioFVM_vector.h"

#include <iostream>
#include <fstream>
#include <omp.h>
#include <fenv.h>
#define xyz_to_n(x, y, z, sizeX, sizeY) (( z*sizeY + y )*sizeX + x)

namespace BioFVM
{

	// inline double xyz_to_n(int x, int y, int z, int sizeX, int sizeY)
	// {
	// 	return (( z*sizeY + y )*sizeX + x);
	// }

	void heterogeneous_diffusion_solver_omp(Microenvironment &M, double dt)
	{
		//std::vector<BioFVM::gradient> density_vector_copy = *M.p_density_vectors;
		if (M.mesh.uniform_mesh == false)
		{
			std::cout << "Error: This algorithm is written for uniform Cartesian meshes. Try: something else." << std::endl
					  << std::endl;
			return;
		}

		int sizeX = M.mesh.x_coordinates.size();
		int sizeY = M.mesh.y_coordinates.size();
		int sizeZ = M.mesh.z_coordinates.size();

		for (unsigned int substrat = 0; substrat < M.number_of_densities(); substrat++)
		{
			//retrieve vector values
			std::vector<double> density_map(M.number_of_voxels(), 0.0);
			
			#pragma omp parallel for
			for (unsigned int i = 0; i < (*M.p_density_vectors).size(); i++)
			{
				/*
				if((*M.p_density_vectors)[i][substrat]<1e-50){
					density_map[i] = 0.000001; //WARNING : NEGATIVE VALUES ARE REPLACED BY ZERO
				}
				else{
					density_map[i] = (*M.p_density_vectors)[i][substrat]; //WARNING : NEGATIVE VALUES ARE REPLACED BY ZERO
				}*/
					density_map[i] = std::max(0.0, (*M.p_density_vectors)[i][substrat]); //WARNING : NEGATIVE VALUES ARE REPLACED BY ZERO
			}

			//retrieve diffusion values
			std::vector<double> diffusion_map(M.number_of_voxels(), 0.0);
			diffusion_map=M.diffusion_coeff_map[substrat];
			
			/*
			std::copy(M.diffusion_coeff_map[substrat].begin(),M.diffusion_coeff_map[substrat].end(),diffusion_map.begin());
			//#pragma omp parallel for
			for (unsigned int i = 0; i < (M.diffusion_coeff_map[substrat]).size(); i++)
			{
				diffusion_map[i] = M.diffusion_coeff_map[substrat][i];
				//diffusion_map[i] = 100000.0;//+M.cartesian_indices(i)[1]*10.0; //continuous gradient in Y direction
				//diffusion_map[i] = 59000.0-M.cartesian_indices(i)[1]*300.0; //continuous gradient in Y direction
			}
			*/

			//retrieve boundary conditions
			std::vector<double> dirichlet_condition(M.number_of_voxels(), -1.0);
			if (M.borders_set != true)
				{
					M.apply_dirichlet_conditions();
				}
		
			for (unsigned int i = 0; i < M.borders.size(); i++)
			{
					double dirichlet_value=M.dirichlet_value_vectors[M.borders[i]][substrat];
					if (M.dirichlet_activation_vectors[M.borders[i]][substrat] == true && dirichlet_value>0.0) //limitation of current implementation should be stricly superior to 0; (neumann is defined as zero in dirichlet_value if another substrat have a dirichlet conditions)
					{
						dirichlet_condition[M.borders[i]] = dirichlet_value;
					}
			}

			//solve2D(density_map, diffusion_map, dirichlet_condition,0.0, dt,sizeX, sizeY, M.mesh.dx, M.mesh.dy);
			if(default_microenvironment_options.simulate_2D==false){
			solve3D_variable(density_map, diffusion_map, dirichlet_condition,0.0, dt,sizeX, sizeY, sizeZ, M.mesh.dx, M.mesh.dy,M.mesh.dz);
			}
			else{
			solve2D_variable(density_map, diffusion_map, dirichlet_condition,0.0, dt,sizeX, sizeY, M.mesh.dx, M.mesh.dy);
			}
			//std::cout<<density_map[xyz_to_n(0,0,0,sizeX,sizeY)]<<std::endl;
		
			#pragma omp parallel for
			for (unsigned int i = 0; i < (*M.p_density_vectors).size(); i++)
			{
				if(density_map[i]<0.0){
					density_map[i]=0.0;
				}
				(*M.p_density_vectors)[i][substrat] = density_map[i];
			}
		}
		M.apply_dirichlet_conditions();
		M.diffusion_solver_setup_done = false;
		//(*M.p_density_vectors) = (*Mghost.p_density_vectors);
	}



	void solve2D(std::vector<double>& density, std::vector<double> diffusion_coeff_map, std::vector<double> dirichlet_conditions, double decay_rate, double dt, int sizeX, int sizeY, double dx, double dy)
	{
		std::vector<double> M_copy = std::vector<double>(density.size(), 0.0);
		//X-DIFFUSION
		#pragma omp parallel for
		for (unsigned int j = 0; j < sizeY; j++)
		{
			std::vector<double> Coeff_A(sizeX, 0.0);
			std::vector<double> Coeff_B(sizeX, 0.0);
			std::vector<double> Coeff_C(sizeX, 0.0);
			std::vector<double> Coeff_D(sizeX, 0.0);
			std::vector<double> p(sizeX, 0.0);
			std::vector<double> q(sizeX, 0.0);
			std::string left_boundary_1D="neumann";
			std::string right_boundary_1D="neumann";

			if(dirichlet_conditions[xyz_to_n(0,j,0,sizeX,sizeY)]>-1.0){
				left_boundary_1D="dirichlet";
			}
			if(dirichlet_conditions[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)]>-1.0){
				right_boundary_1D="dirichlet";
			}
			for (unsigned int i = 0; i < sizeX; i++)
			{
				unsigned int n = xyz_to_n(i,j,0,sizeX,sizeY);

				double lambdax = (diffusion_coeff_map[n] * dt / 2.0) / pow(dx, 2.0);
				double lambday = (diffusion_coeff_map[n] * dt / 2.0) / pow(dy, 2.0);
				Coeff_A[i] = -lambdax;
				Coeff_B[i] = 1.0 + 2.0 * lambdax;
				Coeff_C[i] = -lambdax;
				//‖bi‖>‖ai‖+‖ci‖ stability conditions http://www.industrial-maths.com/ms6021_thomas.pdf

				double jplus, jminus;
				if (j == 0)
				{
					jminus = 1;
				}
				else
				{
					jminus = j - 1;
				}
				if (j == sizeY - 1)
				{
					jplus = sizeY - 2;
				}
				else
				{
					jplus = j + 1;
				}
				
				Coeff_D[i] = (1.0 - decay_rate * dt / 2.0) * density[n] + lambday * (density[xyz_to_n(i,jplus,0,sizeX,sizeY)] - 2.0 * density[n] + density[xyz_to_n(i,jminus,0,sizeX,sizeY)]);


				if (std::isnan(Coeff_D[i]))
				{
					std::cout << "NAN VALUE INTRODUCED" << std::endl;
					
				}
			}

			//p0 & q0
			if(left_boundary_1D=="dirichlet"){
				p[0] = 0.0; // (A0+C0)/B0
				q[0] = dirichlet_conditions[xyz_to_n(0,j,0,sizeX,sizeY)];			
			}
			else{
				p[0] = (Coeff_A[0] + Coeff_C[0]) / Coeff_B[0]; // (A0+C0)/B0
				//p[0] = (Coeff_C[0]) / Coeff_B[0]; // (C0)/B0
				q[0] = Coeff_D[0] / Coeff_B[0];
			}

			//pi & qi
			for (unsigned int i = 1; i < sizeX; i++) // PIERRE
			{
				p[i] = Coeff_C[i] / (Coeff_B[i] - Coeff_A[i] * p[i - 1]);
				q[i] = (Coeff_D[i] - Coeff_A[i] * q[i - 1]) / (Coeff_B[i] - Coeff_A[i] * p[i - 1]);
			}

			//Wm
			if(right_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)];
			}
			else{
				M_copy[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)] = (q[sizeX - 1] - p[sizeX - 1] * q[sizeX - 2]) / (1.0 - p[sizeX - 1] * p[sizeX - 2]);
				//M_copy[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)] = (q[sizeX - 1]);
			}

			//Wm-1 to W1
			for (int i = sizeX - 2; i > 0; i--)
			{
				//M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[i] - density[xyz_to_n(i+1,j,0,sizeX,sizeY)] * p[i];
				M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[i] - M_copy[xyz_to_n(i+1,j,0,sizeX,sizeY)] * p[i];
			}

			//W0
			if(left_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(0,j,0,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(0,j,0,sizeX,sizeY)];
			}
			else{
				//M_copy[xyz_to_n(0,j,0,sizeX,sizeY)] =q[0] - density[xyz_to_n(1,j,0,sizeX,sizeY)] * p[0];
				M_copy[xyz_to_n(0,j,0,sizeX,sizeY)] =q[0] - M_copy[xyz_to_n(1,j,0,sizeX,sizeY)] * p[0];
			}

		}
		density = M_copy;

		//Y-DIFFUSION
		#pragma omp parallel for
		for (unsigned int i = 0; i < sizeX; i++)
		{
			std::vector<double> Coeff_A(sizeY, 0.0);
			std::vector<double> Coeff_B(sizeY, 0.0);
			std::vector<double> Coeff_C(sizeY, 0.0);
			std::vector<double> Coeff_D(sizeY, 0.0);
			std::vector<double> p(sizeY, 0.0);
			std::vector<double> q(sizeY, 0.0);
			std::string left_boundary_1D="neumann";
			std::string right_boundary_1D="neumann";
			
			if(dirichlet_conditions[xyz_to_n(i,0,0,sizeX,sizeY)]>-1.0){
				left_boundary_1D="dirichlet";
			}
			if(dirichlet_conditions[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)]>-1.0){
				right_boundary_1D="dirichlet";
			}
			for (unsigned int j = 0; j < sizeY; j++)
			{
				unsigned int n = xyz_to_n(i,j,0,sizeX,sizeY);

				double lambdax = (diffusion_coeff_map[n] * dt / 2.0) / pow(dx, 2.0);
				double lambday = (diffusion_coeff_map[n] * dt / 2.0) / pow(dy, 2.0);
				Coeff_A[j] = -lambday;
				Coeff_B[j] = 1.0 + 2.0 * lambday;
				Coeff_C[j] = -lambday;

				double iplus, iminus;
				if (i == 0)
				{
					iminus = 1;
				}
				else
				{
					iminus = i - 1;
				}
				if (i == sizeX - 1)
				{
					iplus = sizeX - 2;
				}
				else
				{
					iplus = i + 1;
				}

				Coeff_D[j] = (1.0 - decay_rate * dt / 2.0) * density[n] + lambdax * (density[xyz_to_n(iplus,j,0,sizeX,sizeY)] - 2.0 * density[n] + density[xyz_to_n(iminus,j,0,sizeX,sizeY)]);
				

				if (std::isnan(Coeff_D[j]))
				{
					std::cout << "NAN VALUE INTRODUCED" << std::endl;
					
				}
			}
			//p0 & q0
			if(left_boundary_1D=="dirichlet"){
				p[0] = 0.0; // (A0+C0)/B0
				q[0] = dirichlet_conditions[xyz_to_n(i,0,0,sizeX,sizeY)];			
			}
			else{
				p[0] = (Coeff_A[0] + Coeff_C[0]) / Coeff_B[0]; // (A0+C0)/B0
				//p[0] = (Coeff_C[0]) / Coeff_B[0]; // (C0)/B0
				q[0] = Coeff_D[0] / Coeff_B[0];
			}

			//pi & qi
			for (unsigned int j = 1; j < sizeY; j++) // PIERRE
			{
				p[j] = Coeff_C[j] / (Coeff_B[j] - Coeff_A[j] * p[j - 1]);
				q[j] = (Coeff_D[j] - Coeff_A[j] * q[j - 1]) / (Coeff_B[j] - Coeff_A[j] * p[j - 1]);
			}
			
			//Wm
			if(right_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)];
			}
			else{
				M_copy[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)] = (q[sizeY - 1] - p[sizeY - 1] * q[sizeY - 2]) / (1.0 - p[sizeY - 1] * p[sizeY - 2]);
				//M_copy[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)] = (q[sizeY - 1]);
			}


			//Wm-1 to W1
			for (int j = sizeY - 2; j > 0; j--)
			{
				//M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[j] - density[xyz_to_n(i,j+1,0,sizeX,sizeY)] * p[j];
				M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[j] - M_copy[xyz_to_n(i,j+1,0,sizeX,sizeY)] * p[j];
			}

			//W0
			if(left_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(i,0,0,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(i,0,0,sizeX,sizeY)];
			}
			else{
				//M_copy[xyz_to_n(i,0,0,sizeX,sizeY)] =q[0] - density[xyz_to_n(i,1,0,sizeX,sizeY)] * p[0];
				M_copy[xyz_to_n(i,0,0,sizeX,sizeY)] =q[0] - M_copy[xyz_to_n(i,1,0,sizeX,sizeY)] * p[0];
			}

		}
		//apply_dirichlet_conditions(dirichlet_conditions, M_copy);
		density = M_copy;
		// std::cout << density[xyz_to_n(25,25,0,sizeX,sizeY)]<< std::endl;
		return;
	}

void solve2D_variable(std::vector<double>& density, std::vector<double> diffusion_coeff_map, std::vector<double> dirichlet_conditions, double decay_rate, double dt, int sizeX, int sizeY, double dx, double dy)
	{
		std::vector<double> M_copy = std::vector<double>(density.size(), 0.0);
		//X-DIFFUSION
		#pragma omp parallel for
		for (unsigned int j = 0; j < sizeY; j++)
		{
			std::vector<double> Coeff_A(sizeX, 0.0);
			std::vector<double> Coeff_B(sizeX, 0.0);
			std::vector<double> Coeff_C(sizeX, 0.0);
			std::vector<double> Coeff_D(sizeX, 0.0);
			std::vector<double> p(sizeX, 0.0);
			std::vector<double> q(sizeX, 0.0);
			std::string left_boundary_1D="neumann";
			std::string right_boundary_1D="neumann";

			if(dirichlet_conditions[xyz_to_n(0,j,0,sizeX,sizeY)]>-1.0){
				left_boundary_1D="dirichlet";
			}
			if(dirichlet_conditions[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)]>-1.0){
				right_boundary_1D="dirichlet";
			}
			for (unsigned int i = 0; i < sizeX; i++)
			{
				unsigned int n = xyz_to_n(i,j,0,sizeX,sizeY);

				double iminus = (i==0) ? 1 : i-1;
				double iplus = (i==sizeX-1) ? sizeX-2 : i+1;
				double jminus = (j==0) ? 1 : j-1;
				double jplus = (j==sizeY-1) ? sizeY-2 : j+1;


				double lambdax = (diffusion_coeff_map[n] * dt / 2.0) / pow(dx, 2.0);
				double lambday = (diffusion_coeff_map[n] * dt / 2.0) / pow(dy, 2.0);
				double diff_Dx=(diffusion_coeff_map[xyz_to_n(iplus,j,0,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(iminus,j,0,sizeX,sizeY)])/(2.0*dx);
				double diff_Dy=(diffusion_coeff_map[xyz_to_n(i,jplus,0,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(i,jminus,0,sizeX,sizeY)])/(2.0*dy);

				Coeff_A[i] = -lambdax - diff_Dx*dt/(4.0*dx);
				Coeff_B[i] = 1.0 + 2.0 * lambdax;
				Coeff_C[i] = -lambdax + diff_Dx*dt/(4.0*dx);
				//‖bi‖>‖ai‖+‖ci‖ stability conditions http://www.industrial-maths.com/ms6021_thomas.pdf


				Coeff_D[i] = (1.0 - decay_rate * dt / 2.0) * density[n]
				+ lambday * (density[xyz_to_n(i,jplus,0,sizeX,sizeY)] - 2.0 * density[n] + density[xyz_to_n(i,jminus,0,sizeX,sizeY)])
				+ (dt/2.0)* diff_Dy* (density[xyz_to_n(i,jplus,0,sizeX,sizeY)] - density[xyz_to_n(i,jminus,0,sizeX,sizeY)])/(2.0*dy);
				//+ (dt/2.0)* diff_Dx* (density[xyz_to_n(iplus,j,0,sizeX,sizeY)] - density[xyz_to_n(iminus,j,0,sizeX,sizeY)])/(2.0*dx);


				if (std::isnan(Coeff_D[i]))
				{
					std::cout << "NAN VALUE INTRODUCED" << std::endl;
					
				}
			}

			//p0 & q0
			if(left_boundary_1D=="dirichlet"){
				p[0] = 0.0; // (A0+C0)/B0
				q[0] = dirichlet_conditions[xyz_to_n(0,j,0,sizeX,sizeY)];			
			}
			else{
				p[0] = (Coeff_A[0] + Coeff_C[0]) / Coeff_B[0]; // (A0+C0)/B0
				//p[0] = (Coeff_C[0]) / Coeff_B[0]; // (C0)/B0
				q[0] = Coeff_D[0] / Coeff_B[0];
			}

			//pi & qi
			for (unsigned int i = 1; i < sizeX; i++) // PIERRE
			{
				p[i] = Coeff_C[i] / (Coeff_B[i] - Coeff_A[i] * p[i - 1]);
				q[i] = (Coeff_D[i] - Coeff_A[i] * q[i - 1]) / (Coeff_B[i] - Coeff_A[i] * p[i - 1]);
			}

			//Wm
			if(right_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)];
			}
			else{
				M_copy[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)] = (q[sizeX - 1] - p[sizeX - 1] * q[sizeX - 2]) / (1.0 - p[sizeX - 1] * p[sizeX - 2]);
				//M_copy[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)] = (q[sizeX - 1]);
			}

			//Wm-1 to W1
			for (int i = sizeX - 2; i > 0; i--)
			{
				//M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[i] - density[xyz_to_n(i+1,j,0,sizeX,sizeY)] * p[i];
				M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[i] - M_copy[xyz_to_n(i+1,j,0,sizeX,sizeY)] * p[i];
			}

			//W0
			if(left_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(0,j,0,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(0,j,0,sizeX,sizeY)];
			}
			else{
				//M_copy[xyz_to_n(0,j,0,sizeX,sizeY)] =q[0] - density[xyz_to_n(1,j,0,sizeX,sizeY)] * p[0];
				M_copy[xyz_to_n(0,j,0,sizeX,sizeY)] =q[0] - M_copy[xyz_to_n(1,j,0,sizeX,sizeY)] * p[0];
			}

		}
		density = M_copy;

		//Y-DIFFUSION
		#pragma omp parallel for
		for (unsigned int i = 0; i < sizeX; i++)
		{
			std::vector<double> Coeff_A(sizeY, 0.0);
			std::vector<double> Coeff_B(sizeY, 0.0);
			std::vector<double> Coeff_C(sizeY, 0.0);
			std::vector<double> Coeff_D(sizeY, 0.0);
			std::vector<double> p(sizeY, 0.0);
			std::vector<double> q(sizeY, 0.0);
			std::string left_boundary_1D="neumann";
			std::string right_boundary_1D="neumann";
			
			if(dirichlet_conditions[xyz_to_n(i,0,0,sizeX,sizeY)]>-1.0){
				left_boundary_1D="dirichlet";
			}
			if(dirichlet_conditions[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)]>-1.0){
				right_boundary_1D="dirichlet";
			}
			for (unsigned int j = 0; j < sizeY; j++)
			{
				unsigned int n = xyz_to_n(i,j,0,sizeX,sizeY);
				double iminus = (i==0) ? 1 : i-1;
				double iplus = (i==sizeX-1) ? sizeX-2 : i+1;
				double jminus = (j==0) ? 1 : j-1;
				double jplus = (j==sizeY-1) ? sizeY-2 : j+1;

				double lambdax = (diffusion_coeff_map[n] * dt / 2.0) / pow(dx, 2.0);
				double lambday = (diffusion_coeff_map[n] * dt / 2.0) / pow(dy, 2.0);
				double diff_Dx=(diffusion_coeff_map[xyz_to_n(iplus,j,0,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(iminus,j,0,sizeX,sizeY)])/(2.0*dx);
				double diff_Dy=(diffusion_coeff_map[xyz_to_n(i,jplus,0,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(i,jminus,0,sizeX,sizeY)])/(2.0*dy);

				Coeff_A[j] = -lambday - diff_Dy*dt/(4.0*dy);
				Coeff_B[j] = 1.0 + 2.0 * lambday;
				Coeff_C[j] = -lambday + diff_Dy*dt/(4.0*dy);


				Coeff_D[j] = (1.0 - decay_rate * dt / 2.0) * density[n]
				+ lambdax * (density[xyz_to_n(iplus,j,0,sizeX,sizeY)] - 2.0 * density[n] + density[xyz_to_n(iminus,j,0,sizeX,sizeY)])
				+ (dt/2.0)* diff_Dx* (density[xyz_to_n(iplus,j,0,sizeX,sizeY)] - density[xyz_to_n(iminus,j,0,sizeX,sizeY)])/(2.0*dx);
				//+ (dt/2.0)* diff_Dy* (density[xyz_to_n(i,jplus,0,sizeX,sizeY)] - density[xyz_to_n(i,jminus,0,sizeX,sizeY)])/(2.0*dy);
				

				if (std::isnan(Coeff_D[j]))
				{
					std::cout << "NAN VALUE INTRODUCED" << std::endl;
					
				}
			}
			//p0 & q0
			if(left_boundary_1D=="dirichlet"){
				p[0] = 0.0; // (A0+C0)/B0
				q[0] = dirichlet_conditions[xyz_to_n(i,0,0,sizeX,sizeY)];			
			}
			else{
				p[0] = (Coeff_A[0] + Coeff_C[0]) / Coeff_B[0]; // (A0+C0)/B0
				//p[0] = (Coeff_C[0]) / Coeff_B[0]; // (C0)/B0
				q[0] = Coeff_D[0] / Coeff_B[0];
			}

			//pi & qi
			for (unsigned int j = 1; j < sizeY; j++) // PIERRE
			{
				p[j] = Coeff_C[j] / (Coeff_B[j] - Coeff_A[j] * p[j - 1]);
				q[j] = (Coeff_D[j] - Coeff_A[j] * q[j - 1]) / (Coeff_B[j] - Coeff_A[j] * p[j - 1]);
			}

			//Wm
			if(right_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)];
			}
			else{
				M_copy[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)] = (q[sizeY - 1] - p[sizeY - 1] * q[sizeY - 2]) / (1.0 - p[sizeY - 1] * p[sizeY - 2]);
				//M_copy[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)] = (q[sizeY - 1]);
			}


			//Wm-1 to W1
			for (int j = sizeY - 2; j > 0; j--)
			{
				//M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[j] - density[xyz_to_n(i,j+1,0,sizeX,sizeY)] * p[j];
				M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[j] - M_copy[xyz_to_n(i,j+1,0,sizeX,sizeY)] * p[j];
			}

			//W0
			if(left_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(i,0,0,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(i,0,0,sizeX,sizeY)];
			}
			else{
				//M_copy[xyz_to_n(i,0,0,sizeX,sizeY)] =q[0] - density[xyz_to_n(i,1,0,sizeX,sizeY)] * p[0];
				M_copy[xyz_to_n(i,0,0,sizeX,sizeY)] =q[0] - M_copy[xyz_to_n(i,1,0,sizeX,sizeY)] * p[0];
			}

		}
		//apply_dirichlet_conditions(dirichlet_conditions, M_copy);
		density = M_copy;
		return;
	}







void solve3D_variable(std::vector<double>& density, std::vector<double> diffusion_coeff_map, std::vector<double> dirichlet_conditions, double decay_rate, double dt, int sizeX, int sizeY, int sizeZ, double dx, double dy, double dz)
	{    
		//EXIT ON NAN
		//feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
		std::vector<double> M_copy = std::vector<double>(density.size(), 0.0);
		//X-DIFFUSION

		//collapse(2) will parrallelize for execution over two dimensions
		#pragma omp parallel for collapse(2)
		for (int k = 0; k < sizeZ; k++){
		for (int j = 0; j < sizeY; j++){
			std::vector<double> Coeff_A(sizeX, 0.0);
			std::vector<double> Coeff_B(sizeX, 0.0);
			std::vector<double> Coeff_C(sizeX, 0.0);
			std::vector<double> Coeff_D(sizeX, 0.0);
			std::vector<double> p(sizeX, 0.0);
			std::vector<double> q(sizeX, 0.0);
			std::string left_boundary_1D="neumann";
			std::string right_boundary_1D="neumann";

			if(dirichlet_conditions[xyz_to_n(0,j,k,sizeX,sizeY)]>-1.0){
				left_boundary_1D="dirichlet";
			}
			if(dirichlet_conditions[xyz_to_n(sizeX-1,j,k,sizeX,sizeY)]>-1.0){
				right_boundary_1D="dirichlet";
			}
			for (unsigned int i = 0; i < sizeX; i++)
			{
				unsigned int n = xyz_to_n(i,j,k,sizeX,sizeY);

				double iminus = (i==0) ? 1 : i-1;
				double iplus = (i==sizeX-1) ? sizeX-2 : i+1;
				double jminus = (j==0) ? 1 : j-1;
				double jplus = (j==sizeY-1) ? sizeY-2 : j+1;
				double kminus = (k==0) ? 1 : k-1;
				double kplus = (k==sizeZ-1) ? sizeZ-2 : k+1;


				double lambdax = (diffusion_coeff_map[n] * dt / 3.0) / pow(dx, 2.0);
				double lambday = (diffusion_coeff_map[n] * dt / 3.0) / pow(dy, 2.0);
				double lambdaz = (diffusion_coeff_map[n] * dt / 3.0) / pow(dz, 2.0);
				double diff_Dx=(diffusion_coeff_map[xyz_to_n(iplus,j,k,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(iminus,j,k,sizeX,sizeY)])/(2.0*dx);
				double diff_Dy=(diffusion_coeff_map[xyz_to_n(i,jplus,k,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(i,jminus,k,sizeX,sizeY)])/(2.0*dy);
				double diff_Dz=(diffusion_coeff_map[xyz_to_n(i,j,kplus,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(i,j,kminus,sizeX,sizeY)])/(2.0*dz);

				Coeff_A[i] = -lambdax - diff_Dx*dt/(6.0*dx);
				Coeff_B[i] = 1.0 + 2.0 * lambdax;
				Coeff_C[i] = -lambdax + diff_Dx*dt/(6.0*dx);
				//‖bi‖>‖ai‖+‖ci‖ stability conditions http://www.industrial-maths.com/ms6021_thomas.pdf

				Coeff_D[i] = (1.0 - decay_rate * dt / 3.0) * density[n]
				+ lambday * (density[xyz_to_n(i,jplus,k,sizeX,sizeY)] - 2.0 * density[n] + density[xyz_to_n(i,jminus,k,sizeX,sizeY)])
				+ lambdaz * (density[xyz_to_n(i,j,kplus,sizeX,sizeY)] - 2.0 * density[n] + density[xyz_to_n(i,j,kminus,sizeX,sizeY)])
				+ (dt/3.0)* diff_Dy* (density[xyz_to_n(i,jplus,k,sizeX,sizeY)] - density[xyz_to_n(i,jminus,k,sizeX,sizeY)])/(2.0*dy)
				+ (dt/3.0)* diff_Dz* (density[xyz_to_n(i,j,kplus,sizeX,sizeY)] - density[xyz_to_n(i,j,kminus,sizeX,sizeY)])/(2.0*dz);


				if (std::isnan(Coeff_D[i]))
				{
					std::cout << "NAN VALUE INTRODUCED" << std::endl;
					//
				}
			}

			//p0 & q0
			if(left_boundary_1D=="dirichlet"){
				p[0] = 0.0; // (A0+C0)/B0
				q[0] = dirichlet_conditions[xyz_to_n(0,j,k,sizeX,sizeY)];			
			}
			else{
				p[0] = (Coeff_A[0] + Coeff_C[0]) / Coeff_B[0]; // (A0+C0)/B0
				//p[0] = (Coeff_C[0]) / Coeff_B[0]; // (C0)/B0
				q[0] = Coeff_D[0] / Coeff_B[0];
			}

			//pi & qi
			for (unsigned int i = 1; i < sizeX; i++) // PIERRE
			{
				p[i] = Coeff_C[i] / (Coeff_B[i] - Coeff_A[i] * p[i - 1]);
				q[i] = (Coeff_D[i] - Coeff_A[i] * q[i - 1]) / (Coeff_B[i] - Coeff_A[i] * p[i - 1]);
			}

			//Wm
			if(right_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(sizeX-1,j,k,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(sizeX-1,j,k,sizeX,sizeY)];
			}
			else{
				M_copy[xyz_to_n(sizeX-1,j,k,sizeX,sizeY)] = (q[sizeX - 1] - p[sizeX - 1] * q[sizeX - 2]) / (1.0 - p[sizeX - 1] * p[sizeX - 2]);
				//M_copy[xyz_to_n(sizeX-1,j,0,sizeX,sizeY)] = (q[sizeX - 1]);
			}

			//Wm-1 to W1
			for (int i = sizeX - 2; i > 0; i--)
			{
				//M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[i] - density[xyz_to_n(i+1,j,0,sizeX,sizeY)] * p[i];
				M_copy[xyz_to_n(i,j,k,sizeX,sizeY)] = q[i] - M_copy[xyz_to_n(i+1,j,k,sizeX,sizeY)] * p[i];
			}

			//W0
			if(left_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(0,j,k,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(0,j,k,sizeX,sizeY)];
			}
			else{
				//M_copy[xyz_to_n(0,j,0,sizeX,sizeY)] =q[0] - density[xyz_to_n(1,j,0,sizeX,sizeY)] * p[0];
				M_copy[xyz_to_n(0,j,k,sizeX,sizeY)] =q[0] - M_copy[xyz_to_n(1,j,k,sizeX,sizeY)] * p[0];
			}

		}
		}
		density = M_copy;

		//Y-DIFFUSION
		#pragma omp parallel for collapse(2)
		for (int i = 0; i < sizeX; i++){
		for (int k = 0; k < sizeZ; k++){
			std::vector<double> Coeff_A(sizeY, 0.0);
			std::vector<double> Coeff_B(sizeY, 0.0);
			std::vector<double> Coeff_C(sizeY, 0.0);
			std::vector<double> Coeff_D(sizeY, 0.0);
			std::vector<double> p(sizeY, 0.0);
			std::vector<double> q(sizeY, 0.0);
			std::string left_boundary_1D="neumann";
			std::string right_boundary_1D="neumann";
			
			if(dirichlet_conditions[xyz_to_n(i,0,k,sizeX,sizeY)]>-1.0){
				left_boundary_1D="dirichlet";
			}
			if(dirichlet_conditions[xyz_to_n(i,sizeY-1,k,sizeX,sizeY)]>-1.0){
				right_boundary_1D="dirichlet";
			}
			for (unsigned int j = 0; j < sizeY; j++)
			{
				unsigned int n = xyz_to_n(i,j,k,sizeX,sizeY);
				double iminus = (i==0) ? 1 : i-1;
				double iplus = (i==sizeX-1) ? sizeX-2 : i+1;
				double jminus = (j==0) ? 1 : j-1;
				double jplus = (j==sizeY-1) ? sizeY-2 : j+1;
				double kminus = (k==0) ? 1 : k-1;
				double kplus = (k==sizeZ-1) ? sizeZ-2 : k+1;

				double lambdax = (diffusion_coeff_map[n] * dt / 3.0) / pow(dx, 2.0);
				double lambday = (diffusion_coeff_map[n] * dt / 3.0) / pow(dy, 2.0);
				double lambdaz = (diffusion_coeff_map[n] * dt / 3.0) / pow(dz, 2.0);
				double diff_Dx=(diffusion_coeff_map[xyz_to_n(iplus,j,k,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(iminus,j,k,sizeX,sizeY)])/(2.0*dx);
				double diff_Dy=(diffusion_coeff_map[xyz_to_n(i,jplus,k,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(i,jminus,k,sizeX,sizeY)])/(2.0*dy);
				double diff_Dz=(diffusion_coeff_map[xyz_to_n(i,j,kplus,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(i,j,kminus,sizeX,sizeY)])/(2.0*dz);

				Coeff_A[j] = -lambday- diff_Dy*dt/(6.0*dy);
				Coeff_B[j] = 1.0 + 2.0 * lambday;
				Coeff_C[j] = -lambday + diff_Dy*dt/(6.0*dy);


				Coeff_D[j] = (1.0 - decay_rate * dt / 3.0) * density[n]
				+ lambdaz * (density[xyz_to_n(i,j,kplus,sizeX,sizeY)] - 2.0 * density[n] + density[xyz_to_n(i,j,kminus,sizeX,sizeY)])
				+ lambdax * (density[xyz_to_n(iplus,j,k,sizeX,sizeY)] - 2.0 * density[n] + density[xyz_to_n(iminus,j,k,sizeX,sizeY)])
				+ (dt/3.0)* diff_Dz* (density[xyz_to_n(i,j,kplus,sizeX,sizeY)] - density[xyz_to_n(i,j,kminus,sizeX,sizeY)])/(2.0*dz);
				+ (dt/3.0)* diff_Dx* (density[xyz_to_n(iplus,j,k,sizeX,sizeY)] - density[xyz_to_n(iminus,j,k,sizeX,sizeY)])/(2.0*dx);

				

				if (std::isnan(Coeff_D[j]))
				{
					std::cout << "NAN VALUE INTRODUCED" << std::endl;
					
				}
			}
			//p0 & q0
			if(left_boundary_1D=="dirichlet"){
				p[0] = 0.0; // (A0+C0)/B0
				q[0] = dirichlet_conditions[xyz_to_n(i,0,k,sizeX,sizeY)];			
			}
			else{
				p[0] = (Coeff_A[0] + Coeff_C[0]) / Coeff_B[0]; // (A0+C0)/B0
				//p[0] = (Coeff_C[0]) / Coeff_B[0]; // (C0)/B0
				q[0] = Coeff_D[0] / Coeff_B[0];
			}

			//pi & qi
			for (unsigned int j = 1; j < sizeY; j++) // PIERRE
			{
				p[j] = Coeff_C[j] / (Coeff_B[j] - Coeff_A[j] * p[j - 1]);
				q[j] = (Coeff_D[j] - Coeff_A[j] * q[j - 1]) / (Coeff_B[j] - Coeff_A[j] * p[j - 1]);
			}

			//Wm
			if(right_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(i,sizeY-1,k,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(i,sizeY-1,k,sizeX,sizeY)];
			}
			else{
				M_copy[xyz_to_n(i,sizeY-1,k,sizeX,sizeY)] = (q[sizeY - 1] - p[sizeY - 1] * q[sizeY - 2]) / (1.0 - p[sizeY - 1] * p[sizeY - 2]);
				//M_copy[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)] = (q[sizeY - 1]);
			}


			//Wm-1 to W1
			for (int j = sizeY - 2; j > 0; j--)
			{
				//M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[j] - density[xyz_to_n(i,j+1,0,sizeX,sizeY)] * p[j];
				M_copy[xyz_to_n(i,j,k,sizeX,sizeY)] = q[j] - M_copy[xyz_to_n(i,j+1,k,sizeX,sizeY)] * p[j];
			}

			//W0
			if(left_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(i,0,k,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(i,0,k,sizeX,sizeY)];
			}
			else{
				//M_copy[xyz_to_n(i,0,0,sizeX,sizeY)] =q[0] - density[xyz_to_n(i,1,0,sizeX,sizeY)] * p[0];
				M_copy[xyz_to_n(i,0,k,sizeX,sizeY)] =q[0] - M_copy[xyz_to_n(i,1,k,sizeX,sizeY)] * p[0];
			}

		}
		}
		density = M_copy;
		
		//Z-DIFFUSION
		#pragma omp parallel for collapse(2)
		for (int j = 0; j < sizeY; j++){
		for (int i = 0; i < sizeX; i++){
			std::vector<double> Coeff_A(sizeZ, 0.0);
			std::vector<double> Coeff_B(sizeZ, 0.0);
			std::vector<double> Coeff_C(sizeZ, 0.0);
			std::vector<double> Coeff_D(sizeZ, 0.0);
			std::vector<double> p(sizeZ, 0.0);
			std::vector<double> q(sizeZ, 0.0);
			std::string left_boundary_1D="neumann";
			std::string right_boundary_1D="neumann";
			
			if(dirichlet_conditions[xyz_to_n(i,j,0,sizeX,sizeY)]>-1.0){
				left_boundary_1D="dirichlet";
			}
			if(dirichlet_conditions[xyz_to_n(i,j,sizeZ-1,sizeX,sizeY)]>-1.0){
				right_boundary_1D="dirichlet";
			}
			for (unsigned int k = 0; k < sizeZ; k++)
			{
				unsigned int n = xyz_to_n(i,j,k,sizeX,sizeY);
				double iminus = (i==0) ? 1 : i-1;
				double iplus = (i==sizeX-1) ? sizeX-2 : i+1;
				double jminus = (j==0) ? 1 : j-1;
				double jplus = (j==sizeY-1) ? sizeY-2 : j+1;
				double kminus = (k==0) ? 1 : k-1;
				double kplus = (k==sizeZ-1) ? sizeZ-2 : k+1;

				double lambdax = (diffusion_coeff_map[n] * dt / 3.0) / pow(dx, 2.0);
				double lambday = (diffusion_coeff_map[n] * dt / 3.0) / pow(dy, 2.0);
				double lambdaz = (diffusion_coeff_map[n] * dt / 3.0) / pow(dz, 2.0);
				double diff_Dx=(diffusion_coeff_map[xyz_to_n(iplus,j,k,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(iminus,j,k,sizeX,sizeY)])/(2.0*dx);
				double diff_Dy=(diffusion_coeff_map[xyz_to_n(i,jplus,k,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(i,jminus,k,sizeX,sizeY)])/(2.0*dy);
				double diff_Dz=(diffusion_coeff_map[xyz_to_n(i,j,kplus,sizeX,sizeY)] - diffusion_coeff_map[xyz_to_n(i,j,kminus,sizeX,sizeY)])/(2.0*dz);
				Coeff_A[k] = -lambdaz- diff_Dz*dt/(6.0*dz);
				Coeff_B[k] = 1.0 + 2.0 * lambdaz;
				Coeff_C[k] = -lambdaz + diff_Dz*dt/(6.0*dz);


				Coeff_D[k] = (1.0 - decay_rate * dt / 3.0) * density[n]
				+ lambday * (density[xyz_to_n(i,jplus,k,sizeX,sizeY)] - 2.0 * density[n] + density[xyz_to_n(i,jminus,k,sizeX,sizeY)])
				+ lambdax * (density[xyz_to_n(iplus,j,k,sizeX,sizeY)] - 2.0 * density[n] + density[xyz_to_n(iminus,j,k,sizeX,sizeY)])
				+ (dt/3.0)* diff_Dy* (density[xyz_to_n(i,jplus,k,sizeX,sizeY)] - density[xyz_to_n(i,jminus,k,sizeX,sizeY)])/(2.0*dy);
				+ (dt/3.0)* diff_Dx* (density[xyz_to_n(iplus,j,k,sizeX,sizeY)] - density[xyz_to_n(iminus,j,k,sizeX,sizeY)])/(2.0*dx);

				if (std::isnan(Coeff_D[k]))
				{
					std::cout << "NAN VALUE INTRODUCED" << std::endl;
					
				}
			}
			//p0 & q0
			if(left_boundary_1D=="dirichlet"){
				p[0] = 0.0; // (A0+C0)/B0
				q[0] = dirichlet_conditions[xyz_to_n(i,j,0,sizeX,sizeY)];			
			}
			else{
				p[0] = (Coeff_A[0] + Coeff_C[0]) / Coeff_B[0]; // (A0+C0)/B0
				//p[0] = (Coeff_C[0]) / Coeff_B[0]; // (C0)/B0
				q[0] = Coeff_D[0] / Coeff_B[0];
			}

			//pi & qi
			for (unsigned int k = 1; k < sizeZ; k++) // PIERRE
			{
				p[k] = Coeff_C[k] / (Coeff_B[k] - Coeff_A[k] * p[k - 1]);
				q[k] = (Coeff_D[k] - Coeff_A[k] * q[k - 1]) / (Coeff_B[k] - Coeff_A[k] * p[k - 1]);			
			}

			//Wm
			if(right_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(i,j,sizeZ-1,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(i,j,sizeZ-1,sizeX,sizeY)];
			}
			else{
				M_copy[xyz_to_n(i,j,sizeZ-1,sizeX,sizeY)] = (q[sizeZ - 1] - p[sizeZ - 1] * q[sizeZ - 2]) / (1.0 - p[sizeZ - 1] * p[sizeZ - 2]);
				//M_copy[xyz_to_n(i,sizeY-1,0,sizeX,sizeY)] = (q[sizeY - 1]);
			}


			//Wm-1 to W1
			for (int k = sizeZ - 2; k > 0; k--)
			{
				//M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = q[j] - density[xyz_to_n(i,j+1,0,sizeX,sizeY)] * p[j];
				M_copy[xyz_to_n(i,j,k,sizeX,sizeY)] = q[k] - M_copy[xyz_to_n(i,j,k+1,sizeX,sizeY)] * p[k];
			}

			//W0
			if(left_boundary_1D=="dirichlet"){
				M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] = dirichlet_conditions[xyz_to_n(i,j,0,sizeX,sizeY)];
			}
			else{
				//M_copy[xyz_to_n(i,0,0,sizeX,sizeY)] =q[0] - density[xyz_to_n(i,1,0,sizeX,sizeY)] * p[0];
				M_copy[xyz_to_n(i,j,0,sizeX,sizeY)] =q[0] - M_copy[xyz_to_n(i,j,1,sizeX,sizeY)] * p[0];
			}
		}
		}
		density=M_copy;
		return;
	}




	// do I even need this?
	void diffusion_decay_solver__constant_coefficients_explicit(Microenvironment &M, double dt)
	{
		static bool precomputations_and_constants_done = false;
		if (!precomputations_and_constants_done)
		{
			std::cout << std::endl
					  << "Using solver: " << __FUNCTION__ << std::endl
					  << "     (constant diffusion coefficient with explicit stepping, implicit decay) ... " << std::endl
					  << std::endl;

			if (M.mesh.uniform_mesh == true)
			{
				std::cout << "Uniform mesh detected! Consider switching to a more efficient method, such as " << std::endl
						  << "     diffusion_decay_solver__constant_coefficients_explicit_uniform_mesh" << std::endl
						  << std::endl;
			}

			precomputations_and_constants_done = true;
		}

		return;
	}

	void diffusion_decay_solver__constant_coefficients_explicit_uniform_mesh(Microenvironment &M, double dt)
	{
		static bool precomputations_and_constants_done = false;
		if (!precomputations_and_constants_done)
		{
			std::cout << std::endl
					  << "Using solver: " << __FUNCTION__ << std::endl
					  << "     (constant diffusion coefficient with explicit stepping, implicit decay, uniform mesh) ... " << std::endl
					  << std::endl;

			if (M.mesh.regular_mesh == false)
			{
				std::cout << "Error. This code is only supported for regular meshes." << std::endl;
			}

			precomputations_and_constants_done = true;
		}

		return;
	}

	void diffusion_decay_solver__constant_coefficients_LOD_3D(Microenvironment &M, double dt)
	{
		if (M.mesh.regular_mesh == false || M.mesh.Cartesian_mesh == false)
		{
			std::cout << "Error: This algorithm is written for regular Cartesian meshes. Try: other solvers!" << std::endl
					  << std::endl;
			return;
		}

		// define constants and pre-computed quantities

		if (!M.diffusion_solver_setup_done)
		{
			std::cout << std::endl
					  << "Using method " << __FUNCTION__ << " (implicit 3-D LOD with Thomas Algorithm) ... "
					  << std::endl
					  << std::endl;

			M.thomas_denomx.resize(M.mesh.x_coordinates.size(), M.zero);
			M.thomas_cx.resize(M.mesh.x_coordinates.size(), M.zero);

			M.thomas_denomy.resize(M.mesh.y_coordinates.size(), M.zero);
			M.thomas_cy.resize(M.mesh.y_coordinates.size(), M.zero);

			M.thomas_denomz.resize(M.mesh.z_coordinates.size(), M.zero);
			M.thomas_cz.resize(M.mesh.z_coordinates.size(), M.zero);

			M.thomas_i_jump = 1;
			M.thomas_j_jump = M.mesh.x_coordinates.size();
			M.thomas_k_jump = M.thomas_j_jump * M.mesh.y_coordinates.size();

			M.thomas_constant1 = M.diffusion_coefficients; // dt*D/dx^2
			M.thomas_constant1a = M.zero;				   // -dt*D/dx^2;
			M.thomas_constant2 = M.decay_rates;			   // (1/3)* dt*lambda
			M.thomas_constant3 = M.one;					   // 1 + 2*constant1 + constant2;
			M.thomas_constant3a = M.one;				   // 1 + constant1 + constant2;

			M.thomas_constant1 *= dt;
			M.thomas_constant1 /= M.mesh.dx;
			M.thomas_constant1 /= M.mesh.dx;

			M.thomas_constant1a = M.thomas_constant1;
			M.thomas_constant1a *= -1.0;

			M.thomas_constant2 *= dt;
			M.thomas_constant2 /= 3.0; // for the LOD splitting of the source

			M.thomas_constant3 += M.thomas_constant1;
			M.thomas_constant3 += M.thomas_constant1;
			M.thomas_constant3 += M.thomas_constant2;

			M.thomas_constant3a += M.thomas_constant1;
			M.thomas_constant3a += M.thomas_constant2;

			// Thomas solver coefficients

			M.thomas_cx.assign(M.mesh.x_coordinates.size(), M.thomas_constant1a);
			M.thomas_denomx.assign(M.mesh.x_coordinates.size(), M.thomas_constant3);
			M.thomas_denomx[0] = M.thomas_constant3a;
			M.thomas_denomx[M.mesh.x_coordinates.size() - 1] = M.thomas_constant3a;
			if (M.mesh.x_coordinates.size() == 1)
			{
				M.thomas_denomx[0] = M.one;
				M.thomas_denomx[0] += M.thomas_constant2;
			}

			M.thomas_cx[0] /= M.thomas_denomx[0];
			for (unsigned int i = 1; i <= M.mesh.x_coordinates.size() - 1; i++)
			{
				axpy(&M.thomas_denomx[i], M.thomas_constant1, M.thomas_cx[i - 1]);
				M.thomas_cx[i] /= M.thomas_denomx[i]; // the value at  size-1 is not actually used
			}

			M.thomas_cy.assign(M.mesh.y_coordinates.size(), M.thomas_constant1a);
			M.thomas_denomy.assign(M.mesh.y_coordinates.size(), M.thomas_constant3);
			M.thomas_denomy[0] = M.thomas_constant3a;
			M.thomas_denomy[M.mesh.y_coordinates.size() - 1] = M.thomas_constant3a;
			if (M.mesh.y_coordinates.size() == 1)
			{
				M.thomas_denomy[0] = M.one;
				M.thomas_denomy[0] += M.thomas_constant2;
			}

			M.thomas_cy[0] /= M.thomas_denomy[0];
			for (unsigned int i = 1; i <= M.mesh.y_coordinates.size() - 1; i++)
			{
				axpy(&M.thomas_denomy[i], M.thomas_constant1, M.thomas_cy[i - 1]);
				M.thomas_cy[i] /= M.thomas_denomy[i]; // the value at  size-1 is not actually used
			}

			M.thomas_cz.assign(M.mesh.z_coordinates.size(), M.thomas_constant1a);
			M.thomas_denomz.assign(M.mesh.z_coordinates.size(), M.thomas_constant3);
			M.thomas_denomz[0] = M.thomas_constant3a;
			M.thomas_denomz[M.mesh.z_coordinates.size() - 1] = M.thomas_constant3a;
			if (M.mesh.z_coordinates.size() == 1)
			{
				M.thomas_denomz[0] = M.one;
				M.thomas_denomz[0] += M.thomas_constant2;
			}

			M.thomas_cz[0] /= M.thomas_denomz[0];
			for (unsigned int i = 1; i <= M.mesh.z_coordinates.size() - 1; i++)
			{
				axpy(&M.thomas_denomz[i], M.thomas_constant1, M.thomas_cz[i - 1]);
				M.thomas_cz[i] /= M.thomas_denomz[i]; // the value at  size-1 is not actually used
			}

			M.diffusion_solver_setup_done = true;
		}

		// x-diffusion

		M.apply_dirichlet_conditions();
#pragma omp parallel for
		for (unsigned int k = 0; k < M.mesh.z_coordinates.size(); k++)
		{
			for (unsigned int j = 0; j < M.mesh.y_coordinates.size(); j++)
			{
				// Thomas solver, x-direction

				// remaining part of forward elimination, using pre-computed quantities
				int n = M.voxel_index(0, j, k);
				(*M.p_density_vectors)[n] /= M.thomas_denomx[0];

				for (unsigned int i = 1; i < M.mesh.x_coordinates.size(); i++)
				{
					n = M.voxel_index(i, j, k);
					axpy(&(*M.p_density_vectors)[n], M.thomas_constant1, (*M.p_density_vectors)[n - M.thomas_i_jump]);
					(*M.p_density_vectors)[n] /= M.thomas_denomx[i];
				}

				for (int i = M.mesh.x_coordinates.size() - 2; i >= 0; i--)
				{
					n = M.voxel_index(i, j, k);
					naxpy(&(*M.p_density_vectors)[n], M.thomas_cx[i], (*M.p_density_vectors)[n + M.thomas_i_jump]);
				}
			}
		}

		// y-diffusion

		M.apply_dirichlet_conditions();
#pragma omp parallel for
		for (unsigned int k = 0; k < M.mesh.z_coordinates.size(); k++)
		{
			for (unsigned int i = 0; i < M.mesh.x_coordinates.size(); i++)
			{
				// Thomas solver, y-direction

				// remaining part of forward elimination, using pre-computed quantities

				int n = M.voxel_index(i, 0, k);
				(*M.p_density_vectors)[n] /= M.thomas_denomy[0];

				for (unsigned int j = 1; j < M.mesh.y_coordinates.size(); j++)
				{
					n = M.voxel_index(i, j, k);
					axpy(&(*M.p_density_vectors)[n], M.thomas_constant1, (*M.p_density_vectors)[n - M.thomas_j_jump]);
					(*M.p_density_vectors)[n] /= M.thomas_denomy[j];
				}

				// back substitution
				// n = voxel_index( mesh.x_coordinates.size()-2 ,j,k);

				for (int j = M.mesh.y_coordinates.size() - 2; j >= 0; j--)
				{
					n = M.voxel_index(i, j, k);
					naxpy(&(*M.p_density_vectors)[n], M.thomas_cy[j], (*M.p_density_vectors)[n + M.thomas_j_jump]);
				}
			}
		}

		// z-diffusion

		M.apply_dirichlet_conditions();
#pragma omp parallel for
		for (unsigned int j = 0; j < M.mesh.y_coordinates.size(); j++)
		{

			for (unsigned int i = 0; i < M.mesh.x_coordinates.size(); i++)
			{
				// Thomas solver, y-direction

				// remaining part of forward elimination, using pre-computed quantities

				int n = M.voxel_index(i, j, 0);
				(*M.p_density_vectors)[n] /= M.thomas_denomz[0];

				// should be an empty loop if mesh.z_coordinates.size() < 2
				for (unsigned int k = 1; k < M.mesh.z_coordinates.size(); k++)
				{
					n = M.voxel_index(i, j, k);
					axpy(&(*M.p_density_vectors)[n], M.thomas_constant1, (*M.p_density_vectors)[n - M.thomas_k_jump]);
					(*M.p_density_vectors)[n] /= M.thomas_denomz[k];
				}

				// back substitution

				// should be an empty loop if mesh.z_coordinates.size() < 2
				for (int k = M.mesh.z_coordinates.size() - 2; k >= 0; k--)
				{
					n = M.voxel_index(i, j, k);
					naxpy(&(*M.p_density_vectors)[n], M.thomas_cz[k], (*M.p_density_vectors)[n + M.thomas_k_jump]);
					// n -= i_jump;
				}
			}
		}

		M.apply_dirichlet_conditions();

		// reset gradient vectors
		//	M.reset_all_gradient_vectors();

		return;
	}

	void diffusion_decay_solver__constant_coefficients_LOD_2D(Microenvironment &M, double dt)
	{
		if (M.mesh.regular_mesh == false)
		{
			std::cout << "Error: This algorithm is written for regular Cartesian meshes. Try: something else." << std::endl
					  << std::endl;
			return;
		}

		// constants for the linear solver (Thomas algorithm)

		if (!M.diffusion_solver_setup_done)
		{
			std::cout << std::endl
					  << "Using method " << __FUNCTION__ << " (2D LOD with Thomas Algorithm) ... " << std::endl
					  << std::endl;

			M.thomas_denomx.resize(M.mesh.x_coordinates.size(), M.zero);
			M.thomas_cx.resize(M.mesh.x_coordinates.size(), M.zero);

			M.thomas_denomy.resize(M.mesh.y_coordinates.size(), M.zero);
			M.thomas_cy.resize(M.mesh.y_coordinates.size(), M.zero);

			// define constants and pre-computed quantities

			M.thomas_i_jump = 1;
			M.thomas_j_jump = M.mesh.x_coordinates.size();

			M.thomas_constant1 = M.diffusion_coefficients; //   dt*D/dx^2
			M.thomas_constant1a = M.zero;				   // -dt*D/dx^2;
			M.thomas_constant2 = M.decay_rates;			   // (1/2)*dt*lambda
			M.thomas_constant3 = M.one;					   // 1 + 2*constant1 + constant2;
			M.thomas_constant3a = M.one;				   // 1 + constant1 + constant2;

			M.thomas_constant1 *= dt;
			M.thomas_constant1 /= M.mesh.dx;
			M.thomas_constant1 /= M.mesh.dx;

			M.thomas_constant1a = M.thomas_constant1;
			M.thomas_constant1a *= -1.0;

			M.thomas_constant2 *= dt;
			M.thomas_constant2 *= 0.5; // for splitting via LOD

			M.thomas_constant3 += M.thomas_constant1;
			M.thomas_constant3 += M.thomas_constant1;
			M.thomas_constant3 += M.thomas_constant2;

			M.thomas_constant3a += M.thomas_constant1;
			M.thomas_constant3a += M.thomas_constant2;

			// Thomas solver coefficients

			M.thomas_cx.assign(M.mesh.x_coordinates.size(), M.thomas_constant1a);
			M.thomas_denomx.assign(M.mesh.x_coordinates.size(), M.thomas_constant3);
			M.thomas_denomx[0] = M.thomas_constant3a;
			M.thomas_denomx[M.mesh.x_coordinates.size() - 1] = M.thomas_constant3a;
			if (M.mesh.x_coordinates.size() == 1)
			{
				M.thomas_denomx[0] = M.one;
				M.thomas_denomx[0] += M.thomas_constant2;
			}

			M.thomas_cx[0] /= M.thomas_denomx[0];
			for (unsigned int i = 1; i <= M.mesh.x_coordinates.size() - 1; i++)
			{
				axpy(&M.thomas_denomx[i], M.thomas_constant1, M.thomas_cx[i - 1]);
				M.thomas_cx[i] /= M.thomas_denomx[i]; // the value at  size-1 is not actually used
			}

			M.thomas_cy.assign(M.mesh.y_coordinates.size(), M.thomas_constant1a);
			M.thomas_denomy.assign(M.mesh.y_coordinates.size(), M.thomas_constant3);
			M.thomas_denomy[0] = M.thomas_constant3a;
			M.thomas_denomy[M.mesh.y_coordinates.size() - 1] = M.thomas_constant3a;
			if (M.mesh.y_coordinates.size() == 1)
			{
				M.thomas_denomy[0] = M.one;
				M.thomas_denomy[0] += M.thomas_constant2;
			}

			M.thomas_cy[0] /= M.thomas_denomy[0];
			for (unsigned int i = 1; i <= M.mesh.y_coordinates.size() - 1; i++)
			{
				axpy(&M.thomas_denomy[i], M.thomas_constant1, M.thomas_cy[i - 1]);
				M.thomas_cy[i] /= M.thomas_denomy[i]; // the value at  size-1 is not actually used
			}

			M.diffusion_solver_setup_done = true;
		}

		// set the pointer

		M.apply_dirichlet_conditions();

// x-diffusion
#pragma omp parallel for
		for (unsigned int j = 0; j < M.mesh.y_coordinates.size(); j++)
		{
			// Thomas solver, x-direction

			// remaining part of forward elimination, using pre-computed quantities
			unsigned int n = M.voxel_index(0, j, 0);
			(*M.p_density_vectors)[n] /= M.thomas_denomx[0];

			n += M.thomas_i_jump;
			for (unsigned int i = 1; i < M.mesh.x_coordinates.size(); i++)
			{
				axpy(&(*M.p_density_vectors)[n], M.thomas_constant1, (*M.p_density_vectors)[n - M.thomas_i_jump]);
				(*M.p_density_vectors)[n] /= M.thomas_denomx[i];
				n += M.thomas_i_jump;
			}

			// back substitution
			n = M.voxel_index(M.mesh.x_coordinates.size() - 2, j, 0);

			for (int i = M.mesh.x_coordinates.size() - 2; i >= 0; i--)
			{
				naxpy(&(*M.p_density_vectors)[n], M.thomas_cx[i], (*M.p_density_vectors)[n + M.thomas_i_jump]);
				n -= M.thomas_i_jump;
			}
		}

		// y-diffusion

		M.apply_dirichlet_conditions();
#pragma omp parallel for
		for (unsigned int i = 0; i < M.mesh.x_coordinates.size(); i++)
		{
			// Thomas solver, y-direction

			// remaining part of forward elimination, using pre-computed quantities

			int n = M.voxel_index(i, 0, 0);
			(*M.p_density_vectors)[n] /= M.thomas_denomy[0];

			n += M.thomas_j_jump;
			for (unsigned int j = 1; j < M.mesh.y_coordinates.size(); j++)
			{
				axpy(&(*M.p_density_vectors)[n], M.thomas_constant1, (*M.p_density_vectors)[n - M.thomas_j_jump]);
				(*M.p_density_vectors)[n] /= M.thomas_denomy[j];
				n += M.thomas_j_jump;
			}

			// back substitution
			n = M.voxel_index(i, M.mesh.y_coordinates.size() - 2, 0);

			for (int j = M.mesh.y_coordinates.size() - 2; j >= 0; j--)
			{
				naxpy(&(*M.p_density_vectors)[n], M.thomas_cy[j], (*M.p_density_vectors)[n + M.thomas_j_jump]);
				n -= M.thomas_j_jump;
			}
		}

		M.apply_dirichlet_conditions();

		// reset gradient vectors
		//	M.reset_all_gradient_vectors();

		return;
	}

	void diffusion_decay_explicit_uniform_rates(Microenvironment &M, double dt)
	{
		using std::cout;
		using std::endl;
		using std::vector;

		// static int n_jump_i = 1;
		// static int n_jump_j = M.mesh.x_coordinates.size();
		// static int n_jump_k = M.mesh.x_coordinates.size() * M.mesh.y_coordinates.size();

		if (!M.diffusion_solver_setup_done)
		{
			M.thomas_i_jump = 1;
			M.thomas_j_jump = M.mesh.x_coordinates.size();
			M.thomas_k_jump = M.thomas_j_jump * M.mesh.y_coordinates.size();

			M.diffusion_solver_setup_done = true;
		}

		if (M.mesh.uniform_mesh == false)
		{
			cout << "Error: This algorithm is written for uniform Cartesian meshes. Try: something else" << endl
				 << endl;
			return;
		}

		// double buffering to reduce memory copy / allocation overhead

		static vector<vector<double>> *pNew = &(M.temporary_density_vectors1);
		static vector<vector<double>> *pOld = &(M.temporary_density_vectors2);

		// swap the buffers

		vector<vector<double>> *pTemp = pNew;
		pNew = pOld;
		pOld = pTemp;
		M.p_density_vectors = pNew;

		// static bool reaction_diffusion_shortcuts_are_set = false;

		static vector<double> constant1 = (1.0 / (M.mesh.dx * M.mesh.dx)) * M.diffusion_coefficients;
		static vector<double> constant2 = dt * constant1;
		static vector<double> constant3 = M.one + dt * M.decay_rates;

		static vector<double> constant4 = M.one - dt * M.decay_rates;

#pragma omp parallel for
		for (unsigned int i = 0; i < (*(M.p_density_vectors)).size(); i++)
		{
			unsigned int number_of_neighbors = M.mesh.connected_voxel_indices[i].size();

			double d1 = -1.0 * number_of_neighbors;

			(*pNew)[i] = (*pOld)[i];
			(*pNew)[i] *= constant4;

			for (unsigned int j = 0; j < number_of_neighbors; j++)
			{
				axpy(&(*pNew)[i], constant2, (*pOld)[M.mesh.connected_voxel_indices[i][j]]);
			}
			vector<double> temp = constant2;
			temp *= d1;
			axpy(&(*pNew)[i], temp, (*pOld)[i]);
		}

		// reset gradient vectors
		//	M.reset_all_gradient_vectors();

		return;
	}

	void diffusion_decay_solver__constant_coefficients_LOD_1D(Microenvironment &M, double dt)
	{
		if (M.mesh.regular_mesh == false)
		{
			std::cout << "Error: This algorithm is written for regular Cartesian meshes. Try: something else." << std::endl
					  << std::endl;
			return;
		}

		// constants for the linear solver (Thomas algorithm)

		if (!M.diffusion_solver_setup_done)
		{
			std::cout << std::endl
					  << "Using method " << __FUNCTION__ << " (2D LOD with Thomas Algorithm) ... " << std::endl
					  << std::endl;

			M.thomas_denomx.resize(M.mesh.x_coordinates.size(), M.zero);
			M.thomas_cx.resize(M.mesh.x_coordinates.size(), M.zero);

			// define constants and pre-computed quantities

			M.thomas_i_jump = 1;
			M.thomas_j_jump = M.mesh.x_coordinates.size();

			M.thomas_constant1 = M.diffusion_coefficients; //   dt*D/dx^2
			M.thomas_constant1a = M.zero;				   // -dt*D/dx^2;
			M.thomas_constant2 = M.decay_rates;			   // (1/2)*dt*lambda
			M.thomas_constant3 = M.one;					   // 1 + 2*constant1 + constant2;
			M.thomas_constant3a = M.one;				   // 1 + constant1 + constant2;

			M.thomas_constant1 *= dt;
			M.thomas_constant1 /= M.mesh.dx;
			M.thomas_constant1 /= M.mesh.dx;

			M.thomas_constant1a = M.thomas_constant1;
			M.thomas_constant1a *= -1.0;

			M.thomas_constant2 *= dt;
			M.thomas_constant2 *= 1; // no splitting via LOD

			M.thomas_constant3 += M.thomas_constant1;
			M.thomas_constant3 += M.thomas_constant1;
			M.thomas_constant3 += M.thomas_constant2;

			M.thomas_constant3a += M.thomas_constant1;
			M.thomas_constant3a += M.thomas_constant2;

			// Thomas solver coefficients

			M.thomas_cx.assign(M.mesh.x_coordinates.size(), M.thomas_constant1a);
			M.thomas_denomx.assign(M.mesh.x_coordinates.size(), M.thomas_constant3);
			M.thomas_denomx[0] = M.thomas_constant3a;
			M.thomas_denomx[M.mesh.x_coordinates.size() - 1] = M.thomas_constant3a;
			if (M.mesh.x_coordinates.size() == 1)
			{
				M.thomas_denomx[0] = M.one;
				M.thomas_denomx[0] += M.thomas_constant2;
			}

			M.thomas_cx[0] /= M.thomas_denomx[0];
			for (unsigned int i = 1; i <= M.mesh.x_coordinates.size() - 1; i++)
			{
				axpy(&M.thomas_denomx[i], M.thomas_constant1, M.thomas_cx[i - 1]);
				M.thomas_cx[i] /= M.thomas_denomx[i]; // the value at  size-1 is not actually used
			}

			M.diffusion_solver_setup_done = true;
		}

		// set the pointer

		M.apply_dirichlet_conditions();

// x-diffusion
#pragma omp parallel for
		for (unsigned int j = 0; j < M.mesh.y_coordinates.size(); j++)
		{
			// Thomas solver, x-direction

			// remaining part of forward elimination, using pre-computed quantities
			unsigned int n = M.voxel_index(0, j, 0);
			(*M.p_density_vectors)[n] /= M.thomas_denomx[0];

			n += M.thomas_i_jump;
			for (unsigned int i = 1; i < M.mesh.x_coordinates.size(); i++)
			{
				axpy(&(*M.p_density_vectors)[n], M.thomas_constant1, (*M.p_density_vectors)[n - M.thomas_i_jump]);
				(*M.p_density_vectors)[n] /= M.thomas_denomx[i];
				n += M.thomas_i_jump;
			}

			// back substitution
			n = M.voxel_index(M.mesh.x_coordinates.size() - 2, j, 0);

			for (int i = M.mesh.x_coordinates.size() - 2; i >= 0; i--)
			{
				naxpy(&(*M.p_density_vectors)[n], M.thomas_cx[i], (*M.p_density_vectors)[n + M.thomas_i_jump]);
				n -= M.thomas_i_jump;
			}
		}

		M.apply_dirichlet_conditions();

		// reset gradient vectors
		//	M.reset_all_gradient_vectors();

		return;
	}

};
