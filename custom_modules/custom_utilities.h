#ifndef __custom_utilities_h__
#define __custom_utilities_h__
#include "../core/PhysiCell.h"
#include "../modules/PhysiCell_standard_modules.h" 
#include <string>
#include <boost/numeric/ublas/matrix.hpp>

using std::string;
using namespace BioFVM; 
using namespace PhysiCell;

void cycle_asynchronicity_complete_cycle(Cell *pCell);
void create_closely_packed_cell(Cell_Definition &cell_definition);
void create_border_cells(Cell_Definition & cell_definition);
void create_closely_packed_cell_with_density(Cell_Definition &cell_definition);
void load_cells_from_file(Cell_Definition &cell_definition, std::string path);
std::vector<int> randomize_fisher_yates(std::vector<int> arr, int n);

std::vector<double> read_cell_var(Cell *pCell, std::vector<string> Variables_names);
void write_cell_var(Cell *pCell, std::vector<string> Variables_names, std::vector<double> values);
void load_new_metabolites(std::vector<string> Var_names, std::vector<double> Init_values);

std::vector<double> load_CSV_vec(string file);
boost::numeric::ublas::matrix<double> load_CSV_mat(string file);
boost::numeric::ublas::matrix<int> load_CSV_mat_int(string file);
std::vector<std::vector<double>> load_CSV_list(string file);
std::vector<double> vectorSumElemWise(std::vector<double>vec1,std::vector<double>vec2);
#endif