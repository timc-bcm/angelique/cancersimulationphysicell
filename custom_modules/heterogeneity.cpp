/*
###############################################################################
# If you use PhysiCell in your project, please cite PhysiCell and the version #
# number, such as below:                                                      #
#                                                                             #
# We implemented and solved the model using PhysiCell (Version x.y.z) [1].    #
#                                                                             #
# [1] A Ghaffarizadeh, R Heiland, SH Friedman, SM Mumenthaler, and P Macklin, #
#     PhysiCell: an Open Source Physics-Based Cell Simulator for Multicellu-  #
#     lar Systems, PLoS Comput. Biol. 14(2): e1005991, 2018                   #
#     DOI: 10.1371/journal.pcbi.1005991                                       #
#                                                                             #
# See VERSION.txt or call get_PhysiCell_version() to get the current version  #
#     x.y.z. Call display_citations() to get detailed information on all cite-#
#     able software used in your PhysiCell application.                       #
#                                                                             #
# Because PhysiCell extensively uses BioFVM, we suggest you also cite BioFVM  #
#     as below:                                                               #
#                                                                             #
# We implemented and solved the model using PhysiCell (Version x.y.z) [1],    #
# with BioFVM [2] to solve the transport equations.                           #
#                                                                             #
# [1] A Ghaffarizadeh, R Heiland, SH Friedman, SM Mumenthaler, and P Macklin, #
#     PhysiCell: an Open Source Physics-Based Cell Simulator for Multicellu-  #
#     lar Systems, PLoS Comput. Biol. 14(2): e1005991, 2018                   #
#     DOI: 10.1371/journal.pcbi.1005991                                       #
#                                                                             #
# [2] A Ghaffarizadeh, SH Friedman, and P Macklin, BioFVM: an efficient para- #
#     llelized diffusive transport solver for 3-D biological simulations,     #
#     Bioinformatics 32(8): 1256-8, 2016. DOI: 10.1093/bioinformatics/btv730  #
#                                                                             #
###############################################################################
#                                                                             #
# BSD 3-Clause License (see https://opensource.org/licenses/BSD-3-Clause)     #
#                                                                             #
# Copyright (c) 2015-2018, Paul Macklin and the PhysiCell Project             #
# All rights reserved.                                                        #
#                                                                             #
# Redistribution and use in source and binary forms, with or without          #
# modification, are permitted provided that the following conditions are met: #
#                                                                             #
# 1. Redistributions of source code must retain the above copyright notice,   #
# this list of conditions and the following disclaimer.                       #
#                                                                             #
# 2. Redistributions in binary form must reproduce the above copyright        #
# notice, this list of conditions and the following disclaimer in the         #
# documentation and/or other materials provided with the distribution.        #
#                                                                             #
# 3. Neither the name of the copyright holder nor the names of its            #
# contributors may be used to endorse or promote products derived from this   #
# software without specific prior written permission.                         #
#                                                                             #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE   #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  #
# POSSIBILITY OF SUCH DAMAGE.                                                 #
#                                                                             #
###############################################################################
*/

#include "heterogeneity.h"
#include "../modules/PhysiCell_settings.h"
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

Simu *simulation;

void create_cell_types(void)
{
	// use the same random seed so that future experiments have the
	// same initial histogram of oncoprotein, even if threading means
	// that future division and other events are still not identical
	// for all runs

	SeedRandom(parameters.ints("random_seed"));

	simulation = get_simu_type();

	// housekeeping
	initialize_default_cell_definition();
	cell_defaults.phenotype.secretion.sync_to_microenvironment(&microenvironment);
	cell_defaults.phenotype.sync_to_functions(cell_defaults.functions);

	//CREATE CELL MODEL
	complete_cycle.code = PhysiCell_constants::custom_cycle_model;
	complete_cycle.name = "Complete cycle model";

	int G0_index = complete_cycle.add_phase(105, "G0");
	int G1_index = complete_cycle.add_phase(106, "G1");
	int S_index = complete_cycle.add_phase(107, "S");
	int G2_index = complete_cycle.add_phase(108, "G2");
	int M_index = complete_cycle.add_phase(109, "M");

	//complete_cycle.data.time_units = "min";
	complete_cycle.phases[M_index].division_at_phase_exit = true;
	complete_cycle.add_phase_link(G0_index, G1_index, NULL); // MAY CHANGE NULL BY A FUNCTION
	complete_cycle.add_phase_link(G1_index, G0_index, NULL); // MAY CHANGE NULL BY A FUNCTION
	complete_cycle.add_phase_link(G1_index, S_index, NULL);
	complete_cycle.add_phase_link(S_index, G2_index, NULL);
	complete_cycle.add_phase_link(G2_index, M_index, NULL);
	complete_cycle.add_phase_link(M_index, G1_index, NULL);

	complete_cycle.transition_rate(G0_index, G1_index) = 0.0;
	complete_cycle.transition_rate(G1_index, G0_index) = 0.0;
	complete_cycle.transition_rate(G1_index, S_index) = 0.00333; // 5 hours in G1
	complete_cycle.transition_rate(S_index, G2_index) = 0.00208; // 8 hours in S
	complete_cycle.transition_rate(G2_index, M_index) = 0.00417; // 4 hours in G2
	complete_cycle.transition_rate(M_index, G1_index) = 0.0167;	 // 1 hour in M

	complete_cycle.phases[0].entry_function = NULL;					  //  ;
	complete_cycle.phases[1].entry_function = NULL;					  //  ;
	complete_cycle.phases[2].entry_function = S_phase_entry_function; // Double nuclear volume ;
	complete_cycle.phases[3].entry_function = NULL;
	complete_cycle.phases[4].entry_function = NULL;


	complete_cycle.default_phase_index = 106;
	cell_defaults.phenotype.cycle.data.current_phase_index = 106;
	complete_cycle.display(std::cout);
	cell_defaults.phenotype.cycle.sync_to_cycle_model(complete_cycle);

	// set the default cell type to no phenotype updates
	cell_defaults.functions.update_phenotype = update_phenotype;

	simulation->init_cell_definitions();

	build_cell_definitions_maps();
	display_cell_definitions(std::cout);

	return;
}

void setup_microenvironment(void)
{
	default_microenvironment_options.use_oxygen_as_first_field = true;
	initialize_microenvironment();
	kernel_gaussian[5] = {0.0};
	calc_gaussian_kernel_1D(kernel_gaussian);
	return;
}

void setup_tissue()
{
	create_closely_packed_cell_with_density(cell_defaults);

	//load_cells_from_file(cell_defaults,"./custom_modules/data_source/Initial_cell_pos/2DMEDIUM.csv");
	simulation->load_metabolites_values();
}

// custom cell phenotype function to scale immunostimulatory factor with hypoxia
void update_phenotype(Cell *pCell, Phenotype &phenotype, double dt)
{
	simulation->update_phenotype(pCell, phenotype, dt);
	return;
}

std::vector<std::string> heterogeneity_coloring_function(Cell *pCell)
{
	static int oncoprotein_i = pCell->custom_data.find_variable_index("oncoprotein");

	static double p_min = parameters.doubles("oncoprotein_min");
	static double p_max = parameters.doubles("oncoprotein_max");

	// immune are black
	std::vector<std::string> output(4, "black");

	if (pCell->type == 1)
	{
		return output;
	}

	// live cells are green, but shaded by oncoprotein value
	if (pCell->phenotype.death.dead == false)
	{
		int oncoprotein = (int)round((1.0 / (p_max - p_min)) * (pCell->custom_data[oncoprotein_i] - p_min) * 255.0);
		char szTempString[128];
		std::sprintf(szTempString, "rgb(%u,%u,%u)", oncoprotein, oncoprotein, 255 - oncoprotein);
		output[0].assign(szTempString);
		output[1].assign(szTempString);

		std::sprintf(szTempString, "rgb(%u,%u,%u)", (int)round(output[0][0] / p_max), (int)round(output[0][1] / p_max), (int)round(output[0][2] / p_max));
		output[2].assign(szTempString);

		return output;
	}

	// if not, dead colors

	if (pCell->phenotype.cycle.current_phase().code == PhysiCell_constants::apoptotic) // Apoptotic - Red
	{
		output[0] = "rgb(255,0,0)";
		output[2] = "rgb(125,0,0)";
	}

	// Necrotic - Brown
	if (pCell->phenotype.cycle.current_phase().code == PhysiCell_constants::necrotic_swelling ||
		pCell->phenotype.cycle.current_phase().code == PhysiCell_constants::necrotic_lysed ||
		pCell->phenotype.cycle.current_phase().code == PhysiCell_constants::necrotic)
	{
		output[0] = "rgb(250,138,38)";
		output[2] = "rgb(139,69,19)";
	}

	return output;
}
