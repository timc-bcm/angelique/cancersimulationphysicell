#include "simu.h"
#include <boost/numeric/odeint.hpp>
using namespace boost::numeric::odeint;
using namespace std;
typedef std::vector<double> state_type;

struct simuKevin::ode
{
	double param;
	double m_param;
	ode(double param) : m_param(param) {}

	void operator()(state_type const &x, state_type &dxdt, double t) const
	{
		dxdt[0] = 0.0;
		dxdt[1] = 0.005 * 140.0 - 0.005 * x[1] * ((pow(15.0 * 1.39e-3, 4.0) / (pow(15.0 * 1.39e-3, 4.0) + pow(x[0], 4.0))) + 40.0 * (pow(x[0], 4.0) / (pow(15.0 * 1.39e-3, 4.0) + pow(x[0], 4.0))));
	}
};

void simuKevin::update_phenotype(Cell *pCell, Phenotype &phenotype, double dt)
    {
        //update_cell_and_death_parameters_O2_based(pCell, phenotype, dt);

        // if cell is dead, don't bother with future phenotype changes.
        if (phenotype.death.dead == true)
        {
            pCell->functions.update_phenotype = NULL;
            return;
        }
        //std::cout << "milliseconds " << ns << "\n" << std::endl;

        // multiply proliferation rate by the oncoprotein

        /*	static int cycle_start_index = live.find_phase_index(PhysiCell_constants::live);
        static int cycle_end_index = live.find_phase_index(PhysiCell_constants::live);
        static int oncoprotein_i = pCell->custom_data.find_variable_index("oncoprotein");

        phenotype.cycle.data.transition_rate(cycle_start_index, cycle_end_index) *= pCell->custom_data[oncoprotein_i];*/

        return;
    }


void simuKevin::ODE_Cells(double dt)
{

#pragma omp parallel for
	for (int i = 0; i < (*all_cells).size(); i++)
	{
		Cell *pCell = (*all_cells)[i];
		if(pCell->phenotype.death.dead==false){

		runge_kutta4<state_type> stepper;
		state_type x = {pCell->nearest_density_vector()[0]/ (microenvironment.mesh.dV * 1e-15), pCell->custom_data[pCell->custom_data.find_variable_index("HIF")]};
		vector<state_type> x_vec;
		vector<double> times;

		//[ integration
		size_t steps = integrate_const(stepper, simuKevin::ode{0.0}, x, 0.0, dt, 0.001);

		pCell->custom_data[pCell->custom_data.find_variable_index("Oxygen")] = x[0]; //x_vec[steps][0];
		pCell->custom_data[pCell->custom_data.find_variable_index("HIF")] = x[1];	 //x_vec[steps][0];
		pCell->custom_data[pCell->custom_data.find_variable_index("Diff_coeff")] = microenvironment.diffusion_coeff_map[0][pCell->get_current_voxel_index()];	 //x_vec[steps][0];

		pCell->phenotype.secretion.net_export_rates[0]=-min(pCell->nearest_density_vector()[0], 1000.0*60.0*2.5e-18 * 50.0);  //mol.cell-1.s-1 -> mmol.cell.min-1   https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3147247/
		}
	}
}

void simuKevin::load_metabolites_values(){

}
void simuKevin::FluxToMicroenv(double dt){

}
void simuKevin::init_cell_definitions()
{
	cell_defaults.parameters.necrosis_type = PhysiCell_constants::deterministic_necrosis;

	// Make sure we're ready for 2D

	cell_defaults.functions.set_orientation = up_orientation;

	cell_defaults.phenotype.geometry.polarity = 1.0;
	//cell_defaults.phenotype.motility.restrict_to_2D = true;

	// use default proliferation and death
	int apoptosis_index = cell_defaults.phenotype.death.find_death_model_index(PhysiCell_constants::apoptosis_death_model);
	cell_defaults.phenotype.death.rates[apoptosis_index] = 0;

	cell_defaults.phenotype.volume.relative_rupture_volume = 0.9;
	cell_defaults.phenotype.death.current_parameters().relative_rupture_volume = 0.9;



	double convert_to_mmol_voxel = 1.39e-3 * (microenvironment.mesh.dV * 1e-15); //mmol/L *L/voxel -> mmol/voxel
	cell_defaults.parameters.o2_proliferation_saturation = cell_defaults.parameters.o2_proliferation_saturation * convert_to_mmol_voxel;
	cell_defaults.parameters.o2_reference = cell_defaults.parameters.o2_reference * convert_to_mmol_voxel;
	cell_defaults.parameters.o2_hypoxic_response = cell_defaults.parameters.o2_hypoxic_response * convert_to_mmol_voxel;
	cell_defaults.parameters.o2_hypoxic_threshold = cell_defaults.parameters.o2_hypoxic_threshold * convert_to_mmol_voxel;
	cell_defaults.parameters.o2_hypoxic_saturation = cell_defaults.parameters.o2_hypoxic_saturation * convert_to_mmol_voxel;
	cell_defaults.parameters.o2_necrosis_threshold = cell_defaults.parameters.o2_necrosis_threshold * convert_to_mmol_voxel;
	cell_defaults.parameters.o2_necrosis_max = cell_defaults.parameters.o2_necrosis_max * convert_to_mmol_voxel;
	cell_defaults.parameters.o2_proliferation_threshold = cell_defaults.parameters.o2_proliferation_threshold * convert_to_mmol_voxel;

	// set default uptake and secretion

	static int oxygen_ID = microenvironment.find_density_index("oxygen"); // 0

	// oxygen
	cell_defaults.phenotype.secretion.secretion_rates[oxygen_ID] =0.0;
	cell_defaults.phenotype.secretion.uptake_rates[oxygen_ID] = 0.0;
	cell_defaults.phenotype.secretion.saturation_densities[oxygen_ID] = 1000.0;
	microenvironment.decay_rates[oxygen_ID] = 0.0;

	// glucose
	cell_defaults.phenotype.secretion.secretion_rates[1] = 0.0;
	cell_defaults.phenotype.secretion.uptake_rates[1] = 1.0;
	cell_defaults.phenotype.secretion.saturation_densities[1] = 1000;

	cell_defaults.name = "cancer cell";
	cell_defaults.type = 0;

	// add custom data
	cell_defaults.custom_data.add_variable("HIF", "dimensionless", 0.005);
	cell_defaults.custom_data.add_variable("Oxygen", "dimensionless", 38.0 * 1.39e-3); // mmHg -> mmol/L
	cell_defaults.phenotype.molecular.internalized_total_substrates[0] = 38.0 * 1.39e-3 * cell_defaults.phenotype.volume.total * 1e-15;
	cell_defaults.custom_data.add_variable("Diff_coeff", "dimensionless",1.0); // mmHg -> mmol/L
}