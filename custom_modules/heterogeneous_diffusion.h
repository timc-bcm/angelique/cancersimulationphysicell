/*

*/
#ifndef HETEROGENEOUS_DIFFUSION_HEADER
#define HETEROGENEOUS_DIFFUSION_HEADER
#include "../core/PhysiCell.h"
#include "../modules/PhysiCell_standard_modules.h"

using namespace BioFVM;
using namespace PhysiCell;

void update_diffusion_map(double dt); //TIMC

void calc_gaussian_kernel_1D(double[5]);

void empty_substrate_at_borders(int substrate, bool &border_setup, std::vector<int>& borders);
extern double kernel_gaussian[5];
extern std::vector<std::vector<Cell*>> diffusion_and_cells_map;
#endif