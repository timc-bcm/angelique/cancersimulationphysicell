#include "simu.h"

Simu* get_simu_type()
{
    Simu *simu;
    int simu_type = parameters.ints("simu_type");
    switch (simu_type)
    {
    case 0:
        simu = new simuPierre();
        break;
    case 1:
        simu = new simuKevin();
        break;
    default :
        simu = new simuKevin();
        break;
    }
    return simu;
}
