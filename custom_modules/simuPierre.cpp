#include "simu.h"

/* BOOST LIBRARY */
#include <boost/numeric/odeint.hpp>
#include <boost/algorithm/algorithm.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/iterator/counting_iterator.hpp>

#include <boost/exception/all.hpp>
#include <boost/throw_exception.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iterator>
//#include <regex>
#include <chrono>

using namespace boost::numeric::odeint;
using namespace std;

/* TYPEDEF */
typedef std::chrono::high_resolution_clock Clock;

auto rng = std::default_random_engine{}; //rng engine
std::vector<string> Variables = {"Akt", "AMPK", "cMyc", "HIF", "mTOR", "NOX", "p53", "PDK", "PI3K", "PTEN", "RAS", "SOD", "VEGF", "GluT1", "HK", "G6PD", "GPI", "PFKFB2", "PFK1", "ALD", "TPI", "GAPDH", "PGK", "PHGDH", "PGAM", "ENO", "PKM2", "PDH", "ACC", "LDH", "Glucose", "G6P", "F6P", "FBP", "G3P", "DHAP", "13BPG", "3PG", "2PG", "PEP", "Pyruvate", "Lactate", "R5P", "F26BP", "Serine", "Citrate", "AMP", "ADP", "ATP", "NAD", "NADH", "complex2", "ROS", "H_secretion", "Oxygen_consumed", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "r16", "r17", "r18", "r19", "r20", "r21", "r22", "r23", "r24", "r25", "r26", "r27", "r28", "r29", "r30"};

std::vector<double> Initial_values = {2.4956, 3.7331, 3.0821, 0.85313, 3.2055, 1.0914, 1.6045, 0.1291, 1.9682, 3.8462, 2.191, 2.1411, 1.7857, 3.9597, 4.8056, 0.20937, 2.6555, 3.6359, 3.8737, 4.2905, 0.075379, 4.8516, 0.19654, 3.4285, 4.3281, 3.9095, 2.6875, 3.5989, 3.3719, 0.21066, 0.99842, 0.91092, 0.88592, 0.93563, 0.57639, 0.77752, 0.25045, 0.86502, 0.14431, 0.98411, 0.9903, 0.9878, 0.85863, 0.19563, 0.35918, 0.40621, 8.375, 1.6571, 1.268, 0.29402, 1.056, 0.46577, 0.67717, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};


void mutateCell(Cell *pCell, Phenotype &phenotype, double dt){
	std::vector<double> Gamma=pCell->custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("GeneGamma")].value;
	for(int i=0; i<Gamma.size(); i++){
		unsigned seed = chrono::system_clock::now().time_since_epoch().count();
		default_random_engine generator(seed);
		double var=0.6;
		//double var=0.00006;
		if(Gamma[i]<1.0){
			var=0.02;
			//var=0.000002;
		}
		normal_distribution<double> distribution(Gamma[i], var);
		double mutation = distribution(generator);
		if(Gamma[i]==0){
			mutation=0;
		}
		else{
			if(Gamma[i] <1.0){
				while(mutation>=1.0 || mutation<=0.0){
					if(mutation>1.0 & mutation<2.0){
						mutation=1-(mutation-1); //ex: mutation = 1.2  => new mutation = 2-1.2 = 0.8
					}
					else if(mutation<0.0 & mutation > -1.0){
						mutation=-mutation; //ex: mutation = -0.2  => new mutation = 0.2						
					}
					else{
						mutation = distribution(generator); //re randomize
					}
				}
			}
			else if(Gamma[i]>1.0){
				while(mutation<=1.0 || mutation>=30.0){
					if(mutation<=1.0){
					mutation= 1+(1-mutation);//ex 0.4 => new mutation= 1+(1-0.4)=1.6 //ex -0.2 => new = 1+(1--0.2)=2.2 
					}
					else if(mutation>=30.0){
					mutation= 30-(mutation-30);//ex 40.4 => new mutation= 40-(40.4-40)=39.6 
					}
					else{
						mutation = distribution(generator);
					}
				}
			}
			else if(Gamma[i] ==1.0){
				mutation = 1.0;
			}
		}
		Gamma[i]=mutation;
	}
	pCell->custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("GeneGamma")].value=Gamma;
}



void simuPierre::update_phenotype(Cell *pCell, Phenotype &phenotype, double dt)
{
	if (phenotype.death.dead == true)
	{
		pCell->functions.update_phenotype = NULL;
		return;
	}

	int G0_code = 105;
	int G1_code = 106;
	int S_code = 107;
	static int G1_phase_index = phenotype.cycle.model().find_phase_index(G1_code);
	static int S_phase_index = phenotype.cycle.model().find_phase_index(S_code);
	static int G0_phase_index = phenotype.cycle.model().find_phase_index(G0_code);
	static int necrosis_index = phenotype.death.find_death_model_index(PhysiCell_constants::necrosis_death_model);

	int current_voxel_index = pCell->get_current_voxel_index();
	pCell->custom_data[pCell->custom_data.find_variable_index("Density")] = pCell->get_microenvironment()->density_map[current_voxel_index];
	pCell->custom_data[pCell->custom_data.find_variable_index("Local_Diffusion_Coeff")] = pCell->get_microenvironment()->diffusion_coeff_map[0][current_voxel_index];
	pCell->custom_data[pCell->custom_data.find_variable_index("Local_voxel")] = pCell->get_current_voxel_index();

	static int oxygen_substrate_index = pCell->get_microenvironment()->find_density_index("oxygen");

	double pO2 = microenvironment.density_vector(current_voxel_index)[0] / (1e-15); // PhysiCell_constants::oxygen_index];
	int n = pCell->phenotype.cycle.current_phase_index();

	// this multiplier is for linear interpolation of the oxygen value
	double multiplier = 1.0;

	/*
	if (pO2 < pCell->parameters.o2_proliferation_saturation)
	{
		multiplier = (pO2 - pCell->parameters.o2_proliferation_threshold) / (pCell->parameters.o2_proliferation_saturation - pCell->parameters.o2_proliferation_threshold);
	}
	if (pO2 < pCell->parameters.o2_proliferation_threshold)
	{
		multiplier = 0.0;

	}


	*/

	//   pCell->custom_data[pCell->custom_data.find_variable_index("Density")]
	double max_allowed_density_3D = 2.5;
	double max_allowed_density_2D = 4.5;
	double max_allowed_density = max_allowed_density_2D;
	if (default_microenvironment_options.simulate_2D == false)
	{
		max_allowed_density = max_allowed_density_3D;
	}
	if (microenvironment.density_map[pCell->get_current_voxel_index()] > max_allowed_density || pCell->custom_data["ATP"] < 0.8)
	{
		if (mechanics_updated_one_time == true && phenotype_updated_one_time == false)
		{
			phenotype.cycle.data.current_phase_index = G0_phase_index;
		}

		phenotype.cycle.data.transition_rate(G1_phase_index, G0_phase_index) = 9e99;
		phenotype.cycle.data.transition_rate(G0_phase_index, G1_phase_index) = 0.0;

		//TODO: QUIESCENT CELL MAY HAVE REDUCED METABOLISM ?
		//pCell->custom_data["dt_solve"] = PhysiCell::ODE_dt / 5.0;
		pCell->custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("metabolites_parameters")].value[84] = cell_defaults.custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("metabolites_parameters")].value[84];
	}
	else
	{
		//phenotype.cycle.data.current_phase_index=1;
		phenotype.cycle.data.transition_rate(G1_phase_index, G0_phase_index) = 0.0;
		phenotype.cycle.data.transition_rate(G0_phase_index, G1_phase_index) = 9e99;
		pCell->custom_data["dt_solve"] = PhysiCell::ODE_dt;
		pCell->custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("metabolites_parameters")].value[84] = cell_defaults.custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("metabolites_parameters")].value[84];
	}


	if (phenotype.cycle.data.current_phase_index == G0_phase_index)
	{
		//NOTE:MAY REENABLE THIS ?
	//	pCell->is_movable = false;
		int randNum = rand()%(100-1+ 1) + 1;
		if(randNum>=99)	{	
			mutateCell(pCell, phenotype, dt);
		}
	}
	// else{
	// 	//NOTE:MAY REENABLE THIS ?
	// //	pCell->is_movable = true;
	// }

	// now, update the appropriate cycle transition rate
	phenotype.cycle.data.transition_rate(G1_phase_index, S_phase_index) = multiplier *
																		  pCell->parameters.pReference_live_phenotype->cycle.data.transition_rate(G1_phase_index, S_phase_index);
	//phenotype.cycle.data.transition_rate(G1_phase_index,G0_phase_index) = G0_entry_rates;
	//phenotype.cycle.data.transition_rate(G0_phase_index,G1_phase_index) = G0_leaving_rates;

	//update_cell_and_death_parameters_O2_based(pCell, phenotype, dt);
	// if cell is dead, don't bother with future phenotype changes.
	// Update necrosis rate
	multiplier = 0.0;
	double ATP_necrosis_threshold = 0.3;
	double ATP_necrosis_max = 1e-5;
	/*if (pCell->custom_data["ATP"] < ATP_necrosis_threshold)
	{
		multiplier = (ATP_necrosis_threshold - pCell->custom_data["ATP"]) / (ATP_necrosis_threshold - ATP_necrosis_max);
	}*/
	if (pCell->phenotype.cycle.data.elapsed_time_in_phase >= 3.0 * 60.0 && pCell->phenotype.cycle.data.current_phase_index == G0_phase_index && pCell->custom_data["ATP"] < ATP_necrosis_threshold)
	{
		multiplier = (ATP_necrosis_threshold - pCell->custom_data["ATP"]) / (ATP_necrosis_threshold - ATP_necrosis_max);
	}

	if (pCell->custom_data["ATP"] <= ATP_necrosis_max)
	{
		multiplier = 1.0;
	}

	// now, update the necrosis rate
	pCell->phenotype.death.rates[necrosis_index] = multiplier * pCell->parameters.max_necrosis_rate;

	// check for deterministic necrosis
	if (pCell->parameters.necrosis_type == PhysiCell_constants::deterministic_necrosis && multiplier > 1e-16)
	{
		pCell->phenotype.death.rates[necrosis_index] = 9e99;
	}

	return;
}

void simuPierre::ODE_Cells(double dt)
{

	microenv_substrat_dept.assign(microenvironment.number_of_voxels(), std::vector<double>(microenvironment.number_of_densities(), 0.0));

	std::vector<double> oxygen_map_depletion(microenvironment.number_of_voxels(), 0.0);
	std::vector<double> glucose_map_depletion(microenvironment.number_of_voxels(), 0.0);
	std::vector<double> lactate_map_depletion(microenvironment.number_of_voxels(), 0.0);
	std::vector<double> H_map_depletion(microenvironment.number_of_voxels(), 0.0);

	std::vector<Cell *> cells_alive;
	for (int i = 0; i < (*all_cells).size(); i++)
	{
		if ((*all_cells)[i]->phenotype.death.dead == false)
		{
			cells_alive.push_back((*all_cells)[i]);
		}
	}
	std::vector<int> cell_ids_random(boost::counting_iterator<int>(0), boost::counting_iterator<int>(cells_alive.size()));
	int n = cell_ids_random.size();
	cell_ids_random = randomize_fisher_yates(cell_ids_random, n);

#pragma omp declare reduction(vec_double_plus: std::vector<double>: std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus <double>())) \
	initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))

#pragma omp parallel for reduction(vec_double_plus: oxygen_map_depletion, glucose_map_depletion, lactate_map_depletion, H_map_depletion)
	for (int i = 0; i < cells_alive.size(); i++)
	{
		Cell *pCell = cells_alive[cell_ids_random[i]];
		//Reset Fluxes
		pCell->custom_data["H_secretion"] = 0.0;
		pCell->custom_data["Oxygen_consumed"] = 0.0;

		for (int flux = 1; flux < 31; flux++)
		{
			string flux_name = "r" + to_string(flux);
			pCell->custom_data[flux_name] = 0.0;
		}

		pCell->custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("metabolites_parameters")].value[5] = pCell->nearest_density_vector()[microenvironment.find_density_index("oxygen")] / (1e-15); //mM -> set Parameter for extracellular oxygen

		pCell->custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("metabolites_parameters")].value[0] = pCell->nearest_density_vector()[microenvironment.find_density_index("glucose")] / (1e-15); //mM -> set Parameter for extracellular glucose

		double lac_out = pCell->nearest_density_vector()[microenvironment.find_density_index("lactate")] / (1e-15);
		pCell->custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("metabolites_parameters")].value[4] = lac_out; //mM -> set Parameter for extracellular lactate

		double Hplus = pCell->nearest_density_vector()[microenvironment.find_density_index("H")] / 1000.0; //mmole/microncube -> mole/L
		Hplus = Hplus / (1e-15);
		if(Hplus<3.16227766e-8){
			Hplus =3.16227766e-8; //sanity check that cell doest not go beyond physiological pH -> limitation of the model.
		}
		pCell->custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("metabolites_parameters")].value[117] = Hplus; // -> set Parameter for extracellular H+

		int current_mic_voxel_id = pCell->get_current_voxel_index();

		//-------------------------------
		// UPDATE PARAMETERS
		//-------------------------------

		std::vector<double> cell_params = pCell->custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("metabolites_parameters")].value;

		std::array<double, NUM_OF_PARAMETERS> cell_array;
		std::copy(cell_params.begin(), cell_params.end(), cell_array.begin());

		//UPDATE VARIABLES
		std::vector<double> vec_x = read_cell_var(pCell, Variables);

		//Grab each variables and convert it into boost::vector
		state_T x(vec_x.size());
		std::copy(vec_x.begin(), vec_x.end(), x.begin());
		std::vector<double> Gamma=pCell->custom_data.vector_variables[pCell->custom_data.find_vector_variable_index("GeneGamma")].value;
		//-------------------------------
		// 	Bind parameters to a new function
		//-------------------------------
		typedef boost::error_info<struct my_tag, std::string> my_tag_error_info;
		auto ode_without_param = std::bind(&simuPierre::odef_opti, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, cell_array,Gamma);
		auto jac_without_param = std::bind(&simuPierre::jacobi, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, cell_array,Gamma);
		try
		{
			size_t steps = integrate_adaptive(make_dense_output<rosenbrock4<double>>(abstol_solve, reltol_solve), make_pair(ode_without_param, jac_without_param), x, 0.0, pCell->custom_data["dt_solve"], dt / 1.0);
		}
		catch (const std::exception &e)
		{

			std::cout << "\n\n"
					  << "Error happened: " << e.what() << "\n\n"
					  << std::endl;
			pCell->die();
			continue;
		}
		//TODO: WARNING: TEST IF VOXEL REACH ZERO WITH THIS DT AND ERASE FLUX FOR CERTAIN CELLS...

		//UPDATE MICROENVIRONMENT
		//dn_e/dt = - dn_i/dt
		//=> dn_e/dt = - V_i * dCi/dt
		//=> dn_i/dt = - V_e * dCe/dt

		//GLUCOSE
		double dx_glucose = x[55] * (1e-15) * pCell->phenotype.volume.total * (1 / dt); // mM/min -> mmol / min
		//LACTATE
		double dx_lactate = x[75] * (1e-15) * pCell->phenotype.volume.total * (1 / dt); // mmol / min

		//H+
		// double Kw=std::pow(10,-14); //#Ka of water
		// double Ka_lac=std::pow(10,-3.86); //#Ka of lactic acid
		// double dx_H = 1000.0*((dx_lactate * dt / (microenvironment.voxels(current_mic_voxel_id).volume))/1000.0)/(Kw/(std::pow(Hplus,2.0)) + 1 - (lac_out/1000.0) * (-Ka_lac)/(std::pow(Ka_lac+Hplus,2.0))) ;

		double dx_H = x[53] * 1000.0 * (1e-15) * pCell->phenotype.volume.total * (1 / dt); // mol / min -> mmol/min -> TODO: CHECK IF *1000 IS RIGHT ???

		//OXYGEN
		double dx_oxygen = x[54] * (1e-15) * pCell->phenotype.volume.total * (1 / dt); // mmol / min

		oxygen_map_depletion[current_mic_voxel_id] += -dx_oxygen * dt / (microenvironment.voxels(current_mic_voxel_id).volume);
		glucose_map_depletion[current_mic_voxel_id] += -dx_glucose * dt / (microenvironment.voxels(current_mic_voxel_id).volume);
		lactate_map_depletion[current_mic_voxel_id] += dx_lactate * dt / (microenvironment.voxels(current_mic_voxel_id).volume);
		//H_map_depletion[current_mic_voxel_id] += dx_H ;
		H_map_depletion[current_mic_voxel_id] += dx_H * dt / (microenvironment.voxels(current_mic_voxel_id).volume);

		//-------------------------------
		// 	COPY ODE VALUES TO PCELL
		//-------------------------------
		std::copy(x.begin(), x.end(), vec_x.begin());
		write_cell_var(pCell, Variables, vec_x);
	}

#pragma omp parallel for
	for (int j = 0; j < microenvironment.number_of_voxels(); j++)
	{
		microenv_substrat_dept[j][0] = oxygen_map_depletion[j];
		microenv_substrat_dept[j][1] = glucose_map_depletion[j];
		microenv_substrat_dept[j][2] = lactate_map_depletion[j];
		microenv_substrat_dept[j][3] = H_map_depletion[j];
	}
}

void simuPierre::FluxToMicroenv(double number_of_steps)
{
	//#pragma omp parallel for
	for (int j = 0; j < microenvironment.number_of_voxels(); j++)
	{
		for (int substrat = 0; substrat < microenvironment.number_of_densities(); substrat++)
		{
			/*if (microenvironment.density_vector(j)[substrat] < -microenv_substrat_dept[j][substrat] / number_of_steps)
			{
				std::cout << "ERROR ON FLUX N° " << substrat << " AT VOXEL: " << j << " AND WITH FLUX VALUE: " << microenv_substrat_dept[j][substrat] / number_of_steps << " FOR VOXEL QUANTITY OF: " << microenvironment.density_vector(j)[substrat] << std::endl;
			}*/
			microenvironment.density_vector(j)[substrat] += max(-microenvironment.density_vector(j)[substrat] * 0.999, microenv_substrat_dept[j][substrat] / number_of_steps);
			microenvironment.density_vector(j)[substrat] = max(0.0, microenvironment.density_vector(j)[substrat]); //here for sanity check
			if(substrat==3 && microenvironment.density_vector(j)[substrat] <3.1622e-20){
				microenvironment.density_vector(j)[substrat] =3.1622e-20;
			}
		}
	}
	//empty_substrate_at_borders(2,simuPierre::dirichlet_borders_set,simuPierre::dirichlet_borders); //empty lactate at borders
}


void CustomLysis(Cell *pCell, Phenotype &phenotype, double dt)
{
	int current_mic_voxel_id = pCell->get_current_voxel_index();
	//Cells have a doubled volume at the beginning of the
	//Oxygen from cells is not realeased because it is considered in this model that O2_extra = O2_intra

	//Glucose from cells
	double gluc_intra_q = pCell->custom_data["Glucose"] * (1e-15) * (pCell->phenotype.volume.total / pCell->phenotype.volume.relative_rupture_volume) / (microenvironment.voxels(current_mic_voxel_id).volume);
	//std::cout<<"Released glucose from cell: "<<gluc_intra_q<<std::endl;

	//Lactate from cells
	double lac_intra_q = pCell->custom_data["Lactate"] * (1e-15) * (pCell->phenotype.volume.total / pCell->phenotype.volume.relative_rupture_volume) / (microenvironment.voxels(current_mic_voxel_id).volume);
	//std::cout<<"Released lactate from cell: "<<lac_intra_q<<std::endl;

	//H+ from cells
	double H_out = (pCell->nearest_density_vector()[microenvironment.find_density_index("H")] / (1e-15)) / 1000.0; // -> M -> mol/L
	double pH_e = -std::log10(H_out);
	double pH_i = 4.5 + 2.8 / (1 + std::exp(-(pH_e - 4.7)));
	double H_intra_c = std::pow(10, -pH_i); // -> Concentration mol/L
	//NOTE: BELOW CALCULUS IS WRONG: MEAN OF TWO PH DOESN'T HAVE A PHYSICAL SIGNIFICATION BUT IT WILL BE OUR PROXY FOR RELEASE OF PH FROM CELL
	double H_intra_q = ((H_intra_c * (pCell->phenotype.volume.total / pCell->phenotype.volume.relative_rupture_volume) / (microenvironment.voxels(current_mic_voxel_id).volume)) + (H_out * (1.0 - pCell->phenotype.volume.total / (microenvironment.voxels(current_mic_voxel_id).volume)))) / 2.0;
	H_intra_q = H_intra_q * (1e-15) * 1000.0;

	microenvironment.density_vector(current_mic_voxel_id)[1] += gluc_intra_q;
	microenvironment.density_vector(current_mic_voxel_id)[2] += lac_intra_q;
	microenvironment.density_vector(current_mic_voxel_id)[3] = H_intra_q;
	standard_lysis_entry_function(pCell, phenotype, dt);
}

void simuPierre::load_metabolites_values()
{
	std::vector<std::vector<double>> variable_matrix = load_CSV_list("custom_modules/data_source/x01.csv");
#pragma omp parallel for
	for (int i = 0; i < (*all_cells).size(); i++)
	{
		Cell *pCell = (*all_cells)[i];
		std::vector<double> variable_values = variable_matrix[rand() % variable_matrix.size()];
		//std::vector<double> variable_values = variable_matrix[18];
		variable_values.push_back(0.0); //"pH_secretion"
		variable_values.push_back(0.0); //"Oxygen_consumed"
		for (int flux = 1; flux < 31; flux++)
		{
			variable_values.push_back(0.0); //"FLUX"
		}
		write_cell_var(pCell, Variables, variable_values);
	}
}

void simuPierre::init_cell_definitions()
{
	cell_defaults.parameters.necrosis_type = PhysiCell_constants::deterministic_necrosis;

	// Make sure we're ready for 2D

	if (default_microenvironment_options.simulate_2D == true)
	{
		cell_defaults.functions.set_orientation = up_orientation;
		cell_defaults.phenotype.geometry.polarity = 1.0;
		cell_defaults.phenotype.motility.restrict_to_2D = true;
	}
	cell_defaults.phenotype.mechanics.cell_BM_adhesion_strength = 0.0;
	cell_defaults.phenotype.mechanics.cell_BM_repulsion_strength = 0.0;
	cell_defaults.phenotype.mechanics.cell_cell_repulsion_strength /= 100;
	cell_defaults.phenotype.mechanics.cell_cell_adhesion_strength /= 100;

	//Mutate on division entry
	cell_defaults.phenotype.cycle.pCycle_Model->phases[4].entry_function=mutateCell;


	// use default proliferation and death
	int apoptosis_index = cell_defaults.phenotype.death.find_death_model_index(PhysiCell_constants::apoptosis_death_model);
	cell_defaults.phenotype.death.rates[apoptosis_index] = 0;

	cell_defaults.phenotype.death.models[cell_defaults.phenotype.death.find_death_model_index(PhysiCell_constants::necrosis_death_model)]->transition_rate(1, 2) = 1.0 / (10.0 * 24.0 * 60.0); // 60 days max ->10 days max

	//NOTE: MAY RE-ENABLE THIS ?
	cell_defaults.phenotype.death.models[cell_defaults.phenotype.death.find_death_model_index(PhysiCell_constants::necrosis_death_model)]->phases[1].entry_function=CustomLysis;
	//cell_defaults.phenotype.volume.relative_rupture_volume = 0.9;
	//cell_defaults.phenotype.death.current_parameters().relative_rupture_volume = 0.9;

	double convert_to_mmol_L = 1.39e-3; //mmol/L
	cell_defaults.parameters.o2_proliferation_saturation = cell_defaults.parameters.o2_proliferation_saturation * convert_to_mmol_L;
	cell_defaults.parameters.o2_reference = cell_defaults.parameters.o2_reference * convert_to_mmol_L;
	cell_defaults.parameters.o2_hypoxic_response = cell_defaults.parameters.o2_hypoxic_response * convert_to_mmol_L;
	cell_defaults.parameters.o2_hypoxic_threshold = cell_defaults.parameters.o2_hypoxic_threshold * convert_to_mmol_L;
	cell_defaults.parameters.o2_hypoxic_saturation = cell_defaults.parameters.o2_hypoxic_saturation * convert_to_mmol_L;
	cell_defaults.parameters.o2_necrosis_threshold = cell_defaults.parameters.o2_necrosis_threshold * convert_to_mmol_L;
	cell_defaults.parameters.o2_necrosis_max = cell_defaults.parameters.o2_necrosis_max * convert_to_mmol_L;
	cell_defaults.parameters.o2_proliferation_threshold = cell_defaults.parameters.o2_proliferation_threshold * convert_to_mmol_L;
	// set default uptake and secretion

	static int oxygen_ID = microenvironment.find_density_index("oxygen"); // 0

	// oxygen
	cell_defaults.phenotype.secretion.secretion_rates[oxygen_ID] = 0.0;
	cell_defaults.phenotype.secretion.uptake_rates[oxygen_ID] = 0.0;
	cell_defaults.phenotype.secretion.saturation_densities[oxygen_ID] = 1000.0;
	microenvironment.decay_rates[oxygen_ID] = 0.0;

	// glucose
	cell_defaults.phenotype.secretion.secretion_rates[1] = 0.0;
	cell_defaults.phenotype.secretion.uptake_rates[1] = 0.0;
	microenvironment.decay_rates[1] = 0.0;
	cell_defaults.phenotype.secretion.saturation_densities[1] = 1000.0;

	// lactate
	cell_defaults.phenotype.secretion.secretion_rates[2] = 0.0;
	cell_defaults.phenotype.secretion.uptake_rates[2] = 0.0;
	microenvironment.decay_rates[2] = 0.0;
	cell_defaults.phenotype.secretion.saturation_densities[2] = 1000.0;

	// H+
	cell_defaults.phenotype.secretion.secretion_rates[2] = 0.0;
	cell_defaults.phenotype.secretion.uptake_rates[2] = 0.0;
	microenvironment.decay_rates[2] = 0.0;
	cell_defaults.phenotype.secretion.saturation_densities[2] = 1000.0;

	cell_defaults.name = "cancer cell";
	cell_defaults.type = 0;
	// add custom data

	microenv_substrat_dept.resize(microenvironment.number_of_voxels(), std::vector<double>(microenvironment.number_of_densities()));
	load_new_metabolites(Variables, Initial_values);
	GammaInitial.resize(53, 53);
	n.resize(53, 53);
	S.resize(53, 53);
	A = load_CSV_vec("custom_modules/data_source/A.csv");
	D = load_CSV_vec("custom_modules/data_source/D.csv");
	GammaInitial = load_CSV_mat("custom_modules/data_source/gamma_publi.csv");
	lx = load_CSV_vec("custom_modules/data_source/lx.csv");
	n = load_CSV_mat_int("custom_modules/data_source/n.csv");
	vector<double> sim_parameters = load_CSV_vec("custom_modules/data_source/parameters_U_mg.csv");
	S = load_CSV_mat("custom_modules/data_source/S.csv");
	cell_defaults.custom_data.add_vector_variable("metabolites_parameters", "dimensionless", sim_parameters); // mmHg -> mmol/L
	cell_defaults.custom_data.add_variable("Density", "dimensionless", 0.0);								  // mmHg -> mmol/L
	cell_defaults.custom_data.add_variable("Local_Diffusion_Coeff", "dimensionless", 0.0);					  // mmHg -> mmol/L
	cell_defaults.custom_data.add_variable("dt_solve", PhysiCell::ODE_dt);
	cell_defaults.custom_data.add_variable("Local_voxel", 0);
	
	//fill each cell with GammaInitial
	std::vector<double> GeneGamma;

	for(boost::numeric::ublas::matrix<double>::iterator1 it1 = GammaInitial.begin1(); it1 != GammaInitial.end1(); ++it1) {
    for(boost::numeric::ublas::matrix<double>::iterator2 it2 = it1.begin(); it2 !=it1.end(); ++it2) {
      GeneGamma.push_back(*it2);
    }
    cout << endl;
  }
	cell_defaults.custom_data.add_vector_variable("GeneGamma","dimensionless",GeneGamma);
}