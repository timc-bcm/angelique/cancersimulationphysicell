import numpy as np
import os
from numpy import genfromtxt
import matplotlib
import matplotlib.pyplot as plt
from collections import OrderedDict
import math
import time
import scipy
from mpmath import mp
from scipy import optimize
from scipy.integrate import solve_ivp
import sympy
from sympy import Eq
from sympy import Min
from sympy.parsing.sympy_parser import parse_expr
from sympy.printing import cxxcode
from sympy import exp, log
import re

mp.dps = 50


#Vm in the forward (Vmf) and reverse (Vmr) reactions in U × (mg cytosolic protein)− 1 =  μmol min−1 × (mg cytosolic protein)− 1
#Km in mM
#"Specific protein quantification using SILAC and other methods have shown that a typical cultured cell may contain up to 130–150 pg of total proteins (Van de Water and Olmsted 1980; Volpe and Eremenko 1970) corresponding to 60–150 mg/ml (Albe et al. 1990) and a total of about 2 × 109 polypeptides (Nagaraj et al. 2011)." -> https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3745260/
#-> Keeping 60mg/ml -> 2,144 e-9 ml/cell -> 1.29e-7 mg/cell -> 130 pg/cell
vmax_scaling = 790

x0 = [1.9128, 0.076182, 0.17389, 1.2717, 0.89652, 0.4827, 1.7275, 1.5844, 0.61654, 0.23537, 0.4271, 0.92638, 1.1565, 0.00042149, 1.7882, 1.689, 0.36698, 0.50369, 1.1855, 0.85218, 0.55933, 0.40438, 1.4378, 0.3236, 0.062461, 1.5323, 0.83828, 1.4145, 1.6219, 1.7911, 1.3466, 0.44163, 0.0052667, 1.4008, 0.9296, 2.088, 0.019433, 0.803, 0.41383, 0.0098667, 0.059, 8.2666, 0.6096, 0.0067667, 0.61213, 0.085533, 6.3956, 2.5343, 2.3701, 0.95053, 0.049467, 0.087633, 0.0081333, 0.0, 0.0,"r1","r2","r3","r4","r5","r6","r7","r8","r9","r10","r11","r12","r13","r14","r15","r16","r17","r18","r19","r20","r21","r22","r23","r24","r25","r26","r27","r28","r29","r30"]  #initial states

A_values = genfromtxt('A.csv', delimiter=',')  #Basis of production rate of gene or enzyme = 0.005
D_values = genfromtxt('D.csv', delimiter=',')  #Degradation of genes/enzyme = 0.005
Gamma_values = genfromtxt('gamma.csv', delimiter=',')  #γ reflect the regulation of X from Y -> matrix of genes regulation
lx_values = genfromtxt('lx.csv', delimiter=',')  #TEMPORARY EXPLANATION: Adjustment factor of metabolite regulation on gene expression.
n_values = genfromtxt('n.csv', delimiter=',')  #Hill coefficient for describing the cooperativity of the interactions
S_values = genfromtxt('S.csv', delimiter=',')  #Threshold of half concentration production
par_values = genfromtxt('parameters_U_mg.csv', delimiter=',')

Variables = ["Akt", "AMPK", "cMyc", "HIF", "mTOR", "NOX", "p53", "PDK", "PI3K", "PTEN", "RAS", "SOD", "VEGF", "GluT1", "HK", "G6PD", "GPI", "PFKFB2", "PFK1", "ALD", "TPI", "GAPDH", "PGK", "PHGDH", "PGAM", "ENO", "PKM2", "PDH", "ACC", "LDH", "Glucose", "G6P", "F6P", "FBP", "G3P", "DHAP", "13BPG", "3PG", "2PG", "PEP", "Pyruvate", "Lactate", "R5P", "F26BP", "Serine", "Citrate", "AMP", "ADP", "ATP", "NAD", "NADH", "complex2", "ROS", "Glucose_uptake", "Lactate_secretion", "Oxygen_consumed","r1","r2","r3","r4","r5","r6","r7","r8","r9","r10","r11","r12","r13","r14","r15","r16","r17","r18","r19","r20","r21","r22","r23","r24","r25","r26","r27","r28","r29","r30"]

Parameters = ["Gluout", "Pi", "bx6PG", "Ery4P", "Lacout", "O2", "Citrate", "Vmf_1", "Keq_1", "Kgluout_1", "Kgluin_1", "Vm_2", "Ka_2", "Kb_2", "Keq_2", "Kp_2", "Kq_2", "Vmf_3", "Kg6p_3", "Vmr_3", "Kf6p_3", "Kery4p_3", "Kfbp_3", "Kpg_3", "Vm_4", "Katp_4", "beta_4", "alfa_4", "Kf26bp_4", "Kf6p_4", "L_4", "Kcit_4", "Kiatp_4", "Kadp_4", "Kfbp_4", "Keq_4", "Vmf_5", "Kfbp_5", "Vmr_5", "Kdhap_5", "Kg3p_5", "Kms_6", "Kmp_6", "Vf_6", "Vr_6", "Vmf_7", "Knad_7", "Kg3p_7", "Kp_7", "Vmr_7", "Kdpg_7", "Knadh_7", "Vmf_8", "alfa_8", "Ka_8", "Kb_8", "Vmr_8", "beta_8", "Kp_8", "Kq_8", "Kms_9", "Kmp_9", "Vf_9", "Vr_9", "Kms_10", "Kmp_10", "Vf_10", "Vr_10", "Vmax_11", "Kpep_11", "Kadp_11", "Keq_11", "Kpyr_11", "Katp_11", "Vmf_12", "alfa_12", "Ka_12", "Kb_12", "Vmr_12", "beta_12", "Kp_12", "Kq_12", "Vm_13", "Km_13", "k_14", "k1_15", "k2_15", "Vf_16", "Vr_16", "Kms_16", "Kmp_16", "Vm_17", "Km_17", "Vm_18", "Km_18", "Vm_19", "Km_19", "Vm_20", "Km_20", "Vmf_21", "Keq_21", "Klacin_21", "Klacout_21", "k1_22", "k2_22", "k_23", "k_24", "k_25", "k_26", "k_27", "k_28", "Vm_29", "Km_29", "k_30", "O2s", "O2gamma", "O2n", "H_out", "pH_eq"]


def fsymbolic(t, x, param, matrix_A, matrix_D, matrix_Gamma, matrix_lx, matrix_n, matrix_S):
    dx = sympy.zeros(85, 1)
    Gluout, Pi, bx6PG, Ery4P, Lacout, O2, Citrate, Vmf_1, Keq_1, Kgluout_1, Kgluin_1, Vm_2, Ka_2, Kb_2, Keq_2, Kp_2, Kq_2, Vmf_3, Kg6p_3, Vmr_3, Kf6p_3, Kery4p_3, Kfbp_3, Kpg_3, Vm_4, Katp_4, beta_4, alfa_4, Kf26bp_4, Kf6p_4, L_4, Kcit_4, Kiatp_4, Kadp_4, Kfbp_4, Keq_4, Vmf_5, Kfbp_5, Vmr_5, Kdhap_5, Kg3p_5, Kms_6, Kmp_6, Vf_6, Vr_6, Vmf_7, Knad_7, Kg3p_7, Kp_7, Vmr_7, Kdpg_7, Knadh_7, Vmf_8, alfa_8, Ka_8, Kb_8, Vmr_8, beta_8, Kp_8, Kq_8, Kms_9, Kmp_9, Vf_9, Vr_9, Kms_10, Kmp_10, Vf_10, Vr_10, Vmax_11, Kpep_11, Kadp_11, Keq_11, Kpyr_11, Katp_11, Vmf_12, alfa_12, Ka_12, Kb_12, Vmr_12, beta_12, Kp_12, Kq_12, Vm_13, Km_13, k_14, k1_15, k2_15, Vf_16, Vr_16, Kms_16, Kmp_16, Vm_17, Km_17, Vm_18, Km_18, Vm_19, Km_19, Vm_20, Km_20, Vmf_21, Keq_21, Klacin_21, Klacout_21, k1_22, k2_22, k_23, k_24, k_25, k_26, k_27, k_28, Vm_29, Km_29, k_30, O2s, O2gamma, O2n, H_out, pH_eq, vmax_scaling = param

    A, D, Gamma, lx, n, S = matrix_A, matrix_D, matrix_Gamma, matrix_lx, matrix_n, matrix_S
    r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30= sympy.symbols("r(1:31)")
    pH_e=sympy.symbols("pH_e")
    pH_i=sympy.symbols("pH_i")
    pH_inhibition=sympy.symbols("pH_inhibition")

    dx[0] = A[0] * (S[1, 0]**n[1, 0] / (S[1, 0]**n[1, 0] + x[1]**n[1, 0]) + Gamma[1, 0] * x[1]**n[1, 0] / (S[1, 0]**n[1, 0] + x[1]**n[1, 0])) * (S[7, 0]**n[7, 0] / (S[7, 0]**n[7, 0] + x[7]**n[7, 0]) + Gamma[7, 0] * x[7]**n[7, 0] / (S[7, 0]**n[7, 0] + x[7]**n[7, 0])) * (S[8, 0]**n[8, 0] / (S[8, 0]**n[8, 0] + x[8]**n[8, 0]) + Gamma[8, 0] * x[8]**n[8, 0] / (S[8, 0]**n[8, 0] + x[8]**n[8, 0])) - D[0] * x[0]

    dx[1] = A[1] * (S[3, 1]**n[3, 1] / (S[3, 1]**n[3, 1] + x[3]**n[3, 1]) + Gamma[3, 1] * x[3]**n[3, 1] / (S[3, 1]**n[3, 1] + x[3]**n[3, 1])) * (S[10, 1]**n[10, 1] / (S[10, 1]**n[10, 1] + x[10]**n[10, 1]) + Gamma[10, 1] * x[10]**n[10, 1] / (S[10, 1]**n[10, 1] + x[10]**n[10, 1])) * (S[12, 1]**n[12, 1] / (S[12, 1]**n[12, 1] + x[12]**n[12, 1]) + Gamma[12, 1] * x[12]**n[12, 1] / (S[12, 1]**n[12, 1] + x[12]**n[12, 1])) * (S[42, 1]**n[42, 1] / (S[42, 1]**n[42, 1] + (x[42] / lx[42])**n[42, 1]) + Gamma[42, 1] * (x[42] / lx[42])**n[42, 1] / (S[42, 1]**n[42, 1] + (x[42] / lx[42])**n[42, 1])) * (S[48, 1]**n[48, 1] / (S[48, 1]**n[48, 1] + ((x[48] / lx[48]) / (x[46] / lx[46]))**n[48, 1]) + Gamma[48, 1] * ((x[48] / lx[48]) / (x[46] / lx[46]))**n[48, 1] / (S[48, 1]**n[48, 1] + ((x[48] / lx[48]) / (x[46] / lx[46]))**n[48, 1])) - D[1] * x[1]

    dx[2] = A[2] * (S[0, 2]**n[0, 2] / (S[0, 2]**n[0, 2] + x[0]**n[0, 2]) + Gamma[0, 2] * x[0]**n[0, 2] / (S[0, 2]**n[0, 2] + x[0]**n[0, 2])) * (S[6, 2]**n[6, 2] / (S[6, 2]**n[6, 2] + x[6]**n[6, 2]) + Gamma[6, 2] * x[6]**n[6, 2] / (S[6, 2]**n[6, 2] + x[6]**n[6, 2])) * (S[52, 2]**n[52, 2] / (S[52, 2]**n[52, 2] + (x[52] / lx[52])**n[52, 2]) + Gamma[52, 2] * (x[52] / lx[52])**n[52, 2] / (S[52, 2]**n[52, 2] + (x[52] / lx[52])**n[52, 2])) - D[2] * x[2]

    dx[3] = A[3] * (S[2, 3]**n[2, 3] / (S[2, 3]**n[2, 3] + x[2]**n[2, 3]) + Gamma[2, 3] * x[2]**n[2, 3] / (S[2, 3]**n[2, 3] + x[2]**n[2, 3])) * (S[4, 3]**n[4, 3] / (S[4, 3]**n[4, 3] + x[4]**n[4, 3]) + Gamma[4, 3] * x[4]**n[4, 3] / (S[4, 3]**n[4, 3] + x[4]**n[4, 3])) * (S[6, 3]**n[6, 3] / (S[6, 3]**n[6, 3] + x[6]**n[6, 3]) + Gamma[6, 3] * x[6]**n[6, 3] / (S[6, 3]**n[6, 3] + x[6]**n[6, 3])) * (S[9, 3]**n[9, 3] / (S[9, 3]**n[9, 3] + x[9]**n[9, 3]) + Gamma[9, 3] * x[9]**n[9, 3] / (S[9, 3]**n[9, 3] + x[9]**n[9, 3])) * (S[10, 3]**n[10, 3] / (S[10, 3]**n[10, 3] + x[10]**n[10, 3]) + Gamma[10, 3] * x[10]**n[10, 3] / (S[10, 3]**n[10, 3] + x[10]**n[10, 3])) * (S[41, 3]**n[41, 3] / (S[41, 3]**n[41, 3] + (x[41] / lx[41])**n[41, 3]) + Gamma[41, 3] * (x[41] / lx[41])**n[41, 3] / (S[41, 3]**n[41, 3] + (x[41] / lx[41])**n[41, 3])) * (S[52, 3]**n[52, 3] / (S[52, 3]**n[52, 3] + (x[52] / lx[52])**n[52, 3]) + Gamma[52, 3] * (x[52] / lx[52])**n[52, 3] / (S[52, 3]**n[52, 3] + (x[52] / lx[52])**n[52, 3])) - D[3] * x[3] * ((O2s**O2n) / (O2**O2n + O2s**O2n) + O2gamma * (O2**O2n) / (O2**O2n + O2s**O2n)) # SHIFTED HILL NEEDED ????: ((O2s**O2n) / (O2**O2n + O2s**O2n) + O2gamma * (O2**O2n) / (O2**O2n + O2s**O2n))

    dx[4] = A[4] * (S[0, 4]**n[0, 4] / (S[0, 4]**n[0, 4] + x[0]**n[0, 4]) + Gamma[0, 4] * x[0]**n[0, 4] / (S[0, 4]**n[0, 4] + x[0]**n[0, 4])) * (S[1, 4]**n[1, 4] / (S[1, 4]**n[1, 4] + x[1]**n[1, 4]) + Gamma[1, 4] * x[1]**n[1, 4] / (S[1, 4]**n[1, 4] + x[1]**n[1, 4])) * (S[8, 4]**n[8, 4] / (S[8, 4]**n[8, 4] + x[8]**n[8, 4]) + Gamma[8, 4] * x[8]**n[8, 4] / (S[8, 4]**n[8, 4] + x[8]**n[8, 4])) - D[4] * x[4]

    dx[5] = A[5] * (S[1, 5]**n[1, 5] / (S[1, 5]**n[1, 5] + x[1]**n[1, 5]) + Gamma[1, 5] * x[1]**n[1, 5] / (S[1, 5]**n[1, 5] + x[1]**n[1, 5])) * (S[3, 5]**n[3, 5] / (S[3, 5]**n[3, 5] + x[3]**n[3, 5]) + Gamma[3, 5] * x[3]**n[3, 5] / (S[3, 5]**n[3, 5] + x[3]**n[3, 5])) * (S[10, 5]**n[10, 5] / (S[10, 5]**n[10, 5] + x[10]**n[10, 5]) + Gamma[10, 5] * x[10]**n[10, 5] / (S[10, 5]**n[10, 5] + x[10]**n[10, 5])) * (S[52, 5]**n[52, 5] / (S[52, 5]**n[52, 5] + (x[52] / lx[52])**n[52, 5]) + Gamma[52, 5] * (x[52] / lx[52])**n[52, 5] / (S[52, 5]**n[52, 5] + (x[52] / lx[52])**n[52, 5])) - D[5] * x[5]

    dx[6] = A[6] * (S[0, 6]**n[0, 6] / (S[0, 6]**n[0, 6] + x[0]**n[0, 6]) + Gamma[0, 6] * x[0]**n[0, 6] / (S[0, 6]**n[0, 6] + x[0]**n[0, 6])) * (S[9, 6]**n[9, 6] / (S[9, 6]**n[9, 6] + x[9]**n[9, 6]) + Gamma[9, 6] * x[9]**n[9, 6] / (S[9, 6]**n[9, 6] + x[9]**n[9, 6])) * (S[11, 6]**n[11, 6] / (S[11, 6]**n[11, 6] + x[11]**n[11, 6]) + Gamma[11, 6] * x[11]**n[11, 6] / (S[11, 6]**n[11, 6] + x[11]**n[11, 6])) - D[6] * x[6]

    dx[7] = A[7] * (S[3, 7]**n[3, 7] / (S[3, 7]**n[3, 7] + x[3]**n[3, 7]) + Gamma[3, 7] * x[3]**n[3, 7] / (S[3, 7]**n[3, 7] + x[3]**n[3, 7])) * (S[6, 7]**n[6, 7] / (S[6, 7]**n[6, 7] + x[6]**n[6, 7]) + Gamma[6, 7] * x[6]**n[6, 7] / (S[6, 7]**n[6, 7] + x[6]**n[6, 7])) * (S[48, 7]**n[48, 7] / (S[48, 7]**n[48, 7] + ((x[48] / lx[48]) / (x[47] / lx[47]))**n[48, 7]) + Gamma[48, 7] * ((x[48] / lx[48]) / (x[47] / lx[47]))**n[48, 7] / (S[48, 7]**n[48, 7] + ((x[48] / lx[48]) / (x[47] / lx[47]))**n[48, 7])) - D[7] * x[7]

    dx[8] = A[8] * (S[9, 8]**n[9, 8] / (S[9, 8]**n[9, 8] + x[9]**n[9, 8]) + Gamma[9, 8] * x[9]**n[9, 8] / (S[9, 8]**n[9, 8] + x[9]**n[9, 8])) * (S[10, 8]**n[10, 8] / (S[10, 8]**n[10, 8] + x[10]**n[10, 8]) + Gamma[10, 8] * x[10]**n[10, 8] / (S[10, 8]**n[10, 8] + x[10]**n[10, 8])) * (S[41, 8]**n[41, 8] / (S[41, 8]**n[41, 8] + (x[41] / lx[41])**n[41, 8]) + Gamma[41, 8] * (x[41] / lx[41])**n[41, 8] / (S[41, 8]**n[41, 8] + (x[41] / lx[41])**n[41, 8])) * (S[52, 8]**n[52, 8] / (S[52, 8]**n[52, 8] + (x[52] / lx[52])**n[52, 8]) + Gamma[52, 8] * (x[52] / lx[52])**n[52, 8] / (S[52, 8]**n[52, 8] + (x[52] / lx[52])**n[52, 8])) - D[8] * x[8]

    dx[9] = A[9] * (S[6, 9]**n[6, 9] / (S[6, 9]**n[6, 9] + x[6]**n[6, 9]) + Gamma[6, 9] * x[6]**n[6, 9] / (S[6, 9]**n[6, 9] + x[6]**n[6, 9])) - D[9] * x[9]

    dx[10] = A[10] * (S[12, 10]**n[12, 10] / (S[12, 10]**n[12, 10] + x[12]**n[12, 10]) + Gamma[12, 10] * x[12]**n[12, 10] / (S[12, 10]**n[12, 10] + x[12]**n[12, 10])) * (S[52, 10]**n[52, 10] / (S[52, 10]**n[52, 10] + (x[52] / lx[52])**n[52, 10]) + Gamma[52, 10] * (x[52] / lx[52])**n[52, 10] / (S[52, 10]**n[52, 10] + (x[52] / lx[52])**n[52, 10])) - D[10] * x[10]

    dx[11] = A[11] * (S[12, 11]**n[12, 11] / (S[12, 11]**n[12, 11] + x[12]**n[12, 11]) + Gamma[12, 11] * x[12]**n[12, 11] / (S[12, 11]**n[12, 11] + x[12]**n[12, 11])) * (S[52, 11]**n[52, 11] / (S[52, 11]**n[52, 11] + (x[52] / lx[52])**n[52, 11]) + Gamma[52, 11] * (x[52] / lx[52])**n[52, 11] / (S[52, 11]**n[52, 11] + (x[52] / lx[52])**n[52, 11])) - D[11] * x[11]

    dx[12] = A[12] * (S[2, 12]**n[2, 12] / (S[2, 12]**n[2, 12] + x[2]**n[2, 12]) + Gamma[2, 12] * x[2]**n[2, 12] / (S[2, 12]**n[2, 12] + x[2]**n[2, 12])) * (S[3, 12]**n[3, 12] / (S[3, 12]**n[3, 12] + x[3]**n[3, 12]) + Gamma[3, 12] * x[3]**n[3, 12] / (S[3, 12]**n[3, 12] + x[3]**n[3, 12])) * (S[8, 12]**n[8, 12] / (S[8, 12]**n[8, 12] + x[8]**n[8, 12]) + Gamma[8, 12] * x[8]**n[8, 12] / (S[8, 12]**n[8, 12] + x[8]**n[8, 12])) * (S[41, 12]**n[41, 12] / (S[41, 12]**n[41, 12] + (x[41] / lx[41])**n[41, 12]) + Gamma[41, 12] * (x[41] / lx[41])**n[41, 12] / (S[41, 12]**n[41, 12] + (x[41] / lx[41])**n[41, 12])) * (S[52, 12]**n[52, 12] / (S[52, 12]**n[52, 12] + (x[52] / lx[52])**n[52, 12]) + Gamma[52, 12] * (x[52] / lx[52])**n[52, 12] / (S[52, 12]**n[52, 12] + (x[52] / lx[52])**n[52, 12])) - D[12] * x[12]

    dx[13] = A[13] * (S[0, 13]**n[0, 13] / (S[0, 13]**n[0, 13] + x[0]**n[0, 13]) + Gamma[0, 13] * x[0]**n[0, 13] / (S[0, 13]**n[0, 13] + x[0]**n[0, 13])) * (S[2, 13]**n[2, 13] / (S[2, 13]**n[2, 13] + x[2]**n[2, 13]) + Gamma[2, 13] * x[2]**n[2, 13] / (S[2, 13]**n[2, 13] + x[2]**n[2, 13])) * (S[3, 13]**n[3, 13] / (S[3, 13]**n[3, 13] + x[3]**n[3, 13]) + Gamma[3, 13] * x[3]**n[3, 13] / (S[3, 13]**n[3, 13] + x[3]**n[3, 13])) * (S[6, 13]**n[6, 13] / (S[6, 13]**n[6, 13] + x[6]**n[6, 13]) + Gamma[6, 13] * x[6]**n[6, 13] / (S[6, 13]**n[6, 13] + x[6]**n[6, 13])) - D[13] * x[13]

    dx[14] = A[14] * (S[0, 14]**n[0, 14] / (S[0, 14]**n[0, 14] + x[0]**n[0, 14]) + Gamma[0, 14] * x[0]**n[0, 14] / (S[0, 14]**n[0, 14] + x[0]**n[0, 14])) * (S[3, 14]**n[3, 14] / (S[3, 14]**n[3, 14] + x[3]**n[3, 14]) + Gamma[3, 14] * x[3]**n[3, 14] / (S[3, 14]**n[3, 14] + x[3]**n[3, 14])) - D[14] * x[14]

    dx[15] = A[15] * (S[3, 15]**n[3, 15] / (S[3, 15]**n[3, 15] + x[3]**n[3, 15]) + Gamma[3, 15] * x[3]**n[3, 15] / (S[3, 15]**n[3, 15] + x[3]**n[3, 15])) * (S[6, 15]**n[6, 15] / (S[6, 15]**n[6, 15] + x[6]**n[6, 15]) + Gamma[6, 15] * x[6]**n[6, 15] / (S[6, 15]**n[6, 15] + x[6]**n[6, 15])) - D[15] * x[15]

    dx[16] = A[16] * (S[3, 16]**n[3, 16] / (S[3, 16]**n[3, 16] + x[3]**n[3, 16]) + Gamma[3, 16] * x[3]**n[3, 16] / (S[3, 16]**n[3, 16] + x[3]**n[3, 16])) - D[16] * x[16]

    dx[17] = A[17] * (S[0, 17]**n[0, 17] / (S[0, 17]**n[0, 17] + x[0]**n[0, 17]) + Gamma[0, 17] * x[0]**n[0, 17] / (S[0, 17]**n[0, 17] + x[0]**n[0, 17])) * (S[1, 17]**n[1, 17] / (S[1, 17]**n[1, 17] + x[1]**n[1, 17]) + Gamma[1, 17] * x[1]**n[1, 17] / (S[1, 17]**n[1, 17] + x[1]**n[1, 17])) * (S[3, 17]**n[3, 17] / (S[3, 17]**n[3, 17] + x[3]**n[3, 17]) + Gamma[3, 17] * x[3]**n[3, 17] / (S[3, 17]**n[3, 17] + x[3]**n[3, 17])) - D[17] * x[17]

    dx[18] = A[18] * (S[3, 18]**n[3, 18] / (S[3, 18]**n[3, 18] + x[3]**n[3, 18]) + Gamma[3, 18] * x[3]**n[3, 18] / (S[3, 18]**n[3, 18] + x[3]**n[3, 18])) * (S[10, 18]**n[10, 18] / (S[10, 18]**n[10, 18] + x[10]**n[10, 18]) + Gamma[10, 18] * x[10]**n[10, 18] / (S[10, 18]**n[10, 18] + x[10]**n[10, 18])) - D[18] * x[18]

    dx[19] = A[19] * (S[3, 19]**n[3, 19] / (S[3, 19]**n[3, 19] + x[3]**n[3, 19]) + Gamma[3, 19] * x[3]**n[3, 19] / (S[3, 19]**n[3, 19] + x[3]**n[3, 19])) - D[19] * x[19]

    dx[20] = A[20] * (S[3, 20]**n[3, 20] / (S[3, 20]**n[3, 20] + x[3]**n[3, 20]) + Gamma[3, 20] * x[3]**n[3, 20] / (S[3, 20]**n[3, 20] + x[3]**n[3, 20])) - D[20] * x[20]

    dx[21] = A[21] * (S[3, 21]**n[3, 21] / (S[3, 21]**n[3, 21] + x[3]**n[3, 21]) + Gamma[3, 21] * x[3]**n[3, 21] / (S[3, 21]**n[3, 21] + x[3]**n[3, 21])) - D[21] * x[21]

    dx[22] = A[22] * (S[3, 22]**n[3, 22] / (S[3, 22]**n[3, 22] + x[3]**n[3, 22]) + Gamma[3, 22] * x[3]**n[3, 22] / (S[3, 22]**n[3, 22] + x[3]**n[3, 22])) - D[22] * x[22]

    dx[23] = A[23] - D[23] * x[23]

    dx[24] = A[24] * (S[3, 24]**n[3, 24] / (S[3, 24]**n[3, 24] + x[3]**n[3, 24]) + Gamma[3, 24] * x[3]**n[3, 24] / (S[3, 24]**n[3, 24] + x[3]**n[3, 24])) - D[24] * x[24]

    dx[25] = A[25] * (S[3, 25]**n[3, 25] / (S[3, 25]**n[3, 25] + x[3]**n[3, 25]) + Gamma[3, 25] * x[3]**n[3, 25] / (S[3, 25]**n[3, 25] + x[3]**n[3, 25])) - D[25] * x[25]

    dx[26] = A[26] * (S[3, 26]**n[3, 26] / (S[3, 26]**n[3, 26] + x[3]**n[3, 26]) + Gamma[3, 26] * x[3]**n[3, 26] / (S[3, 26]**n[3, 26] + x[3]**n[3, 26])) - D[26] * x[26]

    dx[27] = A[27] * (S[7, 27]**n[7, 27] / (S[7, 27]**n[7, 27] + x[7]**n[7, 27]) + Gamma[7, 27] * x[7]**n[7, 27] / (S[7, 27]**n[7, 27] + x[7]**n[7, 27])) * (S[9, 27]**n[9, 27] / (S[9, 27]**n[9, 27] + x[9]**n[9, 27]) + Gamma[9, 27] * x[9]**n[9, 27] / (S[9, 27]**n[9, 27] + x[9]**n[9, 27])) - D[27] * x[27]

    dx[28] = A[28] * (S[1, 28]**n[1, 28] / (S[1, 28]**n[1, 28] + x[1]**n[1, 28]) + Gamma[1, 28] * x[1]**n[1, 28] / (S[1, 28]**n[1, 28] + x[1]**n[1, 28])) * (S[31, 28]**n[31, 28] / (S[31, 28]**n[31, 28] + (x[31] / lx[31])**n[31, 28]) + Gamma[31, 28] * (x[31] / lx[31])**n[31, 28] / (S[31, 28]**n[31, 28] + (x[31] / lx[31])**n[31, 28])) * (S[43, 28]**n[43, 28] / (S[43, 28]**n[43, 28] + (x[43] / lx[43])**n[43, 28]) + Gamma[43, 28] * (x[43] / lx[43])**n[43, 28] / (S[43, 28]**n[43, 28] + (x[43] / lx[43])**n[43, 28])) - D[28] * x[28]

    dx[29] = A[29] * (S[2, 29]**n[2, 29] / (S[2, 29]**n[2, 29] + x[2]**n[2, 29]) + Gamma[2, 29] * x[2]**n[2, 29] / (S[2, 29]**n[2, 29] + x[2]**n[2, 29])) * (S[3, 29]**n[3, 29] / (S[3, 29]**n[3, 29] + x[3]**n[3, 29]) + Gamma[3, 29] * x[3]**n[3, 29] / (S[3, 29]**n[3, 29] + x[3]**n[3, 29])) - D[29] * x[29]

    dx[30] = r1 - r2
    dx[31] = r2 - r3 - r13
    dx[32] = r3 - r4 - r16 + 2 * r22
    dx[33] = r4 - r5
    dx[34] = r5 + r6 - r7 + r22
    dx[35] = r5 - r6
    dx[36] = r7 - r8
    dx[37] = r8 - r9 - r17
    dx[38] = r9 - r10
    dx[39] = r10 - r11
    dx[40] = r11 - r12 - r18
    dx[41] = r12 - r21
    dx[42] = r13 - 3 * r22 - r23
    dx[43] = r16
    dx[44] = r17 - r24
    dx[45] = r18 - r19 - r26 - r30
    dx[46] = -r15  #AMP
    dx[47] = r2 + r4 - r8 - r11 + r14 + 2 * r15 - r18 + 3 * r19 - r25 - 3 * r26 - 1.5 * r27  #ADP
    dx[48] = -r2 - r4 + r8 + r11 - r14 - r15 + r18 - 3 * r19 + r25 + 3 * r26 + 1.5 * r27  #ATP
    dx[49] = -r7 + r12 + r25  #NAD
    dx[50] = r7 - r12 - r25  #NADH
    dx[51] = r18 - r19 + r25 + 4 * r26 - r27 - r28 #complexe 2
    dx[52] = r28 + r29 - r20
    
    #Total flux of protons #BUFFERING CAPACITY: See Van Slyke equation and 10.1021/ed077p1640. everything is expressed here in M (not mM).
    Kw=10**(-14) #Ka of water
    Ka_lac=10**(-3.86) #Ka of lactic acid
    dx[53] = 0#(r21/1000.0)/(Kw/(H_out**2) + 1 - (Lacout/1000.0) * (-Ka_lac)/((Ka_lac+H_out)**2)) 
       
    #OXYGEN
    # IN THIS MODEL THE AUTHORS MODELED ONLY COMPLEX2 and accounted the extra ATP from NADH (in comparison of the 1.5 ATP from FADH2) in each reaction generating the NADH...
    # The missing 1.5 ATP from NADH (which generate 2.5 ATP) is generated in the r27 reaction (complex2 to 4). Thus the stochiometry is 1.5 ATP but the real (biological) value is the average of ATP from NADH and from FADH. For 13 ATP generated per pyruvate 89% come from NADH and 11% from FADH. The real ratio of ATP generated from the steps after complex 2 should be 1.5 * 0.11 + 2.5*0.89 = 2.39 ATP/(complex in ECT). From the same idea, the oxydation of oxygen needed with the flux from complex2 to 4 depends of the proportion of NADH/FADH used. With NADH, P/O ratio = 5 ATP/O2 and with FADH: P/O = 3 ATP/O2. This lead to : NADH: 1 ATP= 1/5 O and FADH: 1ATP= 1/3 O. Keeping the same proportion of NADH/FADH used (0.89/0.11), we have: 1ATP= 0.89 * 1/5 + 0.11*1/3 = 0.215 O2. The resulting formula for oxygen consumption is:  P/O2 ratio *(number of generated ATP from flux coming from complex2 to 4) = 0.215 *(2.39 * r27)
    
    #We note that this would have been much simpler to directly count the correct amount of ATP in a reaction with complex1,2,3,4 and not separate ATP from NADH into 1 + 1.5 ATP in diverse reactions....
    dx[54] = 0.215 *(2.39 * r27)  #ETC Oxygen consumption from ATP production (from the complex2 site to complex4: r27)
    
    dx[55]=r1
    dx[56]=r2
    dx[57]=r3
    dx[58]=r4
    dx[59]=r5
    dx[60]=r6
    dx[61]=r7
    dx[62]=r8
    dx[63]=r9
    dx[64]=r10
    dx[65]=r11
    dx[66]=r12
    dx[67]=r13
    dx[68]=r14
    dx[69]=r15
    dx[70]=r16
    dx[71]=r17
    dx[72]=r18
    dx[73]=r19
    dx[74]=r20
    dx[75]=r21
    dx[76]=r22
    dx[77]=r23
    dx[78]=r24
    dx[79]=r25
    dx[80]=r26
    dx[81]=r27
    dx[82]=r28
    dx[83]=r29
    dx[84]=r30



    #pH regulation of glycolysis according to Xie: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4018627/
    # R2 ?; R3; R4; R5; R6; R7; R10?; R11?
    pH_e = sympy.UnevaluatedExpr(sympy.sympify("-log(H_out, 10)",evaluate=False))
    pH_i = sympy.UnevaluatedExpr(sympy.sympify("4.5 + 2.8 / (1 + exp(-(pH_e - 4.7)))",evaluate=False))
    #pH_inhibition = sympy.UnevaluatedExpr(sympy.sympify("0.1+ 0.90 / (1.0 + exp(-(pH_e - 6.7) / 0.12))",evaluate=False))
    pH_inhibition = 1

    r1 = vmax_scaling * (((Vmf_1 * x[13]) * (Gluout - (x[30]) / (Keq_1))) / ((Kgluout_1) * (1 + (x[30]) / (Kgluin_1)) + Gluout))

    r2 = vmax_scaling * pH_inhibition * ((((Vm_2 * x[14]) / ((Ka_2) * (Kb_2))) * ((x[30]) * (x[48]) - ((x[31]) * (x[47])) / (Keq_2))) / (1 + (x[30]) / (Ka_2) + (x[48]) / (Kb_2) + ((x[30]) * (x[48])) / ((Ka_2) * (Kb_2)) + (x[31]) / (Kp_2) + (x[47]) / (Kq_2) + ((x[31]) * (x[47])) / ((Kp_2) * (Kq_2)) + ((x[30]) * (x[47])) / ((Ka_2) * (Kq_2)) + ((x[31]) * (x[48])) / ((Kp_2) * (Kb_2))))

    r3 = vmax_scaling * pH_inhibition * (((Vmf_3 * x[16]) * ((x[31]) / (Kg6p_3)) - (Vmr_3 * x[16]) * ((x[32]) / (Kf6p_3))) / (1 + (x[31]) / (Kg6p_3) + (x[32]) / (Kf6p_3) + (Ery4P) / (Kery4p_3) + (x[33]) / (Kfbp_3) + (bx6PG) / (Kpg_3)))

    r4 = vmax_scaling * pH_inhibition * (((Vm_4 * x[18]) * ((((x[32]) / (Katp_4)) / (1 + (x[32]) / (Katp_4))) * ((1 + ((beta_4) * (x[43])) / ((alfa_4) * (Kf26bp_4))) / (1 + (x[43]) / ((alfa_4) * (Kf26bp_4)))))) * (((((x[48]) * (1 + (x[43]) / ((alfa_4) * (Kf26bp_4)))) / ((Kf6p_4) * (1 + (x[43]) / (Kf26bp_4)))) * ((1 + ((x[48]) * (1 + (x[43]) / ((alfa_4) * (Kf26bp_4)))) / ((Kf6p_4) * (1 + (x[43]) / (Kf26bp_4))))**(3))) / ((((L_4) * ((1 + (Citrate) / (Kcit_4))**(4))) * ((1 + (x[32]) / (Kiatp_4))**(4))) / ((1 + (x[43]) / (Kf26bp_4))**(4)) + (1 + ((x[48]) * (1 + (x[43]) / ((alfa_4) * (Kf26bp_4)))) / ((Kf6p_4) * (1 + (x[43]) / (Kf26bp_4))))**(4)) - (((x[33]) * (x[47])) / (((Kadp_4) * (Kfbp_4)) * (Keq_4))) / ((x[33]) / (Kadp_4) + (x[47]) / (Kfbp_4) + ((x[33]) * (x[47])) / ((Kadp_4) * (Kfbp_4)) + 1)))

    r5 = vmax_scaling * pH_inhibition * (((Vmf_5 * x[19]) * ((x[33]) / (Kfbp_5)) - (Vmr_5 * x[19]) * (((x[35]) * (x[34])) / ((Kdhap_5) * (Kg3p_5)))) / (1 + (x[33]) / (Kfbp_5) + (x[35]) / (Kdhap_5) + (x[34]) / (Kg3p_5) + ((x[35]) * (x[34])) / ((Kdhap_5) * (Kg3p_5))))

    r6 = vmax_scaling * pH_inhibition * (Vf_6 * x[20] * x[35] - Vr_6 * x[20] * x[34] / (Kms_6 * Kmp_6 + Kmp_6 * x[35] + Kms_6 * x[34]))

    r7 = vmax_scaling * pH_inhibition * (((Vmf_7 * x[21]) * ((((x[49]) * (x[34])) * (Pi)) / (((Knad_7) * (Kg3p_7)) * (Kp_7))) - (Vmr_7 * x[21]) * (((x[36]) * (x[50])) / ((Kdpg_7) * (Knadh_7)))) / (1 + (x[49]) / (Knad_7) + ((x[49]) * (x[34])) / ((Knad_7) * (Kg3p_7)) + (((x[49]) * (x[34])) * (Pi)) / (((Knad_7) * (Kg3p_7)) * (Kp_7)) + ((x[36]) * (x[50])) / ((Kdpg_7) * (Knadh_7)) + (x[50]) / (Knadh_7)))

    r8 = vmax_scaling * (((Vmf_8 * x[22]) * (((x[36]) * (x[47])) / (((alfa_8) * (Ka_8)) * (Kb_8))) - (Vmr_8 * x[22]) * (((x[37]) * (x[48])) / (((beta_8) * (Kp_8)) * (Kq_8)))) / (1 + (x[36]) / (Ka_8) + (x[47]) / (Kb_8) + ((x[36]) * (x[47])) / (((alfa_8) * (Ka_8)) * (Kb_8)) + ((x[37]) * (x[48])) / (((beta_8) * (Kp_8)) * (Kq_8)) + (x[37]) / (Kp_8) + (x[48]) / (Kq_8)))

    r9 = vmax_scaling * (Vf_9 * x[24] * x[37] - Vr_9 * x[24] * x[38] / (Kms_9 * Kmp_9 + Kmp_9 * x[37] + Kms_9 * x[38]))

    r10 = vmax_scaling * pH_inhibition * (Vf_10 * x[25] * x[38] - Vr_10 * x[25] * x[39] / (Kms_10 * Kmp_10 + Kmp_10 * x[38] + Kms_10 * x[39]))

    r11 = vmax_scaling * pH_inhibition * (((Vmax_11 * x[26]) * (((x[39]) * (x[47])) / ((Kpep_11) * (Kadp_11)) - ((x[40]) * (x[48])) / (((Kpep_11) * (Kadp_11)) * (Keq_11)))) / ((1 + (x[39]) / (Kpep_11) + (x[40]) / (Kpyr_11)) * (1 + (x[47]) / (Kadp_11) + (x[48]) / (Katp_11))))

    r12 = vmax_scaling * (((Vmf_12 * x[29]) * (((x[50]) * (x[40])) / (((alfa_12) * (Ka_12)) * (Kb_12))) - (Vmr_12 * x[29]) * (((x[41]) * (x[49])) / (((beta_12) * (Kp_12)) * (Kq_12)))) / (1 + (x[50]) / (Ka_12) + (x[40]) / (Kb_12) + ((x[50]) * (x[40])) / (((alfa_12) * (Ka_12)) * (Kb_12)) + ((x[41]) * (x[49])) / (((beta_12) * (Kp_12)) * (Kq_12)) + (x[41]) / (Kp_12) + (x[49]) / (Kq_12)))

    r13 = vmax_scaling* pH_inhibition * (Vm_13 * x[15] * (x[31] / (x[31] + Km_13)))

    r14 = (k_14 * x[48])

    r15 = (k1_15 * x[48] * x[46] - k2_15 * x[47] * x[47])

    r16 = vmax_scaling* pH_inhibition * (Vf_16 * x[17] * x[32] - Vr_16 * x[17] * x[43] / (Kms_16 * Kmp_16 + Kmp_16 * x[32] + Kms_16 * x[43]))

    r17 = vmax_scaling * (Vm_17 * x[23] * (x[37] / (x[37] + Km_17)))

    r18 = vmax_scaling * (Vm_18 * x[27] * (x[40] / (x[40] + Km_18)))

    r19 = vmax_scaling * (Vm_19 * x[28] * (x[45] / (x[45] + Km_19)))

    r20 = vmax_scaling * (Vm_20 * x[11] * (x[52] / (x[52] + Km_20)))


    coefa=2
    coefb=2

    #FluxLac=2/(1 + exp(-coefa*(x[41]- Lacout))) - 1
    FluxPH=1 - 1/(1+exp(-coefb*(pH_e-pH_i/pH_eq)))

    #https://www.desmos.com/calculator/xk2khfwysc?lang=fr
    ratio_lac_pyr=(x[41]+0.001)/(0.001+x[40])
    regulation_lac_pyr=1/(1+1/((exp(-10+ratio_lac_pyr))**2)) * (1/(1+exp(150*(0.1-x[40])))) # ratio_lac_pyr must be superior to 10 and pyruvate level must be superior to 0.1 mM
    lac_f=(1/(1+exp(-(x[41]-Lacout)*coefa))) * (x[41]/(0.01+x[41])) *regulation_lac_pyr
    lac_r=0.2*(1 - 1/(1+exp(-(x[41]-Lacout)*coefa))) * (Lacout/(0.1+Lacout)) * FluxPH
    #r21 = vmax_scaling*(Vmf_21)*(lac_f - lac_r)#NOTE:TIMC
 
    r21 = vmax_scaling * (((Vmf_21) * (x[41] - (Lacout) / (Keq_21))) / ((Klacin_21) * (1 + (Lacout) / (Klacout_21)) + x[41]))
    #TODO: REWORK ON R21 STABILITY
    #r21 = Vmf_21 *((-(exp(5 * (-pH_e * pH_eq + pH_i) / pH_eq) + 1.0) * (exp(2 * Lacout - 2 * x[41]) + 1.0) + 2.0 * exp(5 * (-pH_e * pH_eq + pH_i) / pH_eq) + 2.0 * exp(2 * Lacout - 2 * x[41]) + 2.0) / ((exp(5 * (-pH_e * pH_eq + pH_i) / pH_eq) + 1.0) * (exp(2 * Lacout - 2 * x[41]) + 1.0)))*(x[41]/(x[41]+0.01))

    r22 = (k1_22 * x[42] * x[42] * x[42] - k2_22 * x[32] * x[32] * x[34])

    r23 = pH_inhibition * (k_23 * x[42])

    r24 = (k_24 * x[44])

    r25 = (k_25 * x[50])

    r26 =  (k_26 * x[45])

    r27 = (k_27 * x[51])

    r28 = (k_28 * x[51] * O2)

    r29 = vmax_scaling * (Vm_29 * x[5] * (O2 / (O2 + Km_29)))

    r30 = (k_30 * x[45])


    return sympy.flatten(dx),[r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, pH_e, pH_i, pH_inhibition]


# initial conditions
u0 = x0

t = np.linspace(0, 1000000, 1000000)  # time grid

u = sympy.IndexedBase("u", real=True)

#create symbolic vars for each variable
var_list = sympy.symbols("x[0:85]", real=True, positive=True)
par_list = sympy.symbols(list(Parameters) + ["vmax_scaling"], real=True, positive=True)
matrix_A = sympy.symbols("A[0:53]", real=True, positive=True)
matrix_D = sympy.symbols("D[0:53]", real=True, positive=True)
matrix_lx = sympy.symbols("lx[0:53]", real=True, positive=True)
matrix_Gamma = sympy.IndexedBase("Gamma", shape=(53, 53), real=True, positive=True)
matrix_n = sympy.IndexedBase("n", shape=(53, 53), real=True, positive=True)
matrix_S = sympy.IndexedBase("S", shape=(53, 53), real=True, positive=True)


class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)


flux_symbols=fsymbolic(None, var_list, par_list, matrix_A, matrix_D, matrix_Gamma, matrix_lx, matrix_n, matrix_S)[1]
r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, pH_e, pH_i, pH_inhibition=flux_symbols
Matrix = sympy.Matrix(fsymbolic(None, var_list, par_list, matrix_A, matrix_D, matrix_Gamma, matrix_lx, matrix_n, matrix_S)[0])

substitution_dict={}
substitution_dict["pH_e"]=pH_e
substitution_dict["pH_i"]=pH_i
substitution_dict["pH_inhibition"]=pH_inhibition
for flux in range(len(flux_symbols)-3):
    substitution_dict["r"+str(flux+1)]=flux_symbols[flux]
        #Matrix[eq] = sympy.simplify(Matrix[eq]) //TODO: MAY RESIMPLIFY ????
Matrix_symp = Matrix
Matrix_substituted=Matrix.subs(substitution_dict)
jac = Matrix_substituted.jacobian(var_list)
jac_symp = jac
"""
if os.path.exists("ODEeqs.txt"):
    os.remove("ODEeqs.txt")
with open('ODEeqs.txt', 'w') as f:
    for i in range(len(Matrix_symp)):
        print("dxdt[" + str(i) + "]=" + cxxcode(Matrix_symp[i], standard='C++17') + ";", file=f)
    for i in range(len(Matrix_symp)):
        print("dxdt[" + str(i) + "]=" + cxxcode(Matrix_symp[i], standard='C++17') + ";", file=f)


if os.path.exists("jacobian.txt"):
    os.remove("jacobian.txt")
with open('jacobian.txt', 'w') as f:
    for i in range(len(jac_symp.row(0))):
        for j in range(len(jac_symp.col(0))):
            if (cxxcode(sympy.flatten(jac_symp.row(i).col(j))[0], standard='C++17') != "0"):
                print("J(" + str(i) + "," + str(j) + ") = " + cxxcode(sympy.flatten(jac_symp.row(i).col(j))[0], standard='C++17') + ";", file=f)
# print("}",file=f)
"""


cpp_header="""

#include "simu.h"

/* BOOST LIBRARY */

#include <boost/numeric/odeint.hpp>
#include <boost/algorithm/algorithm.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>

#include <string>
#include <vector>
#include <iterator>
//#include <regex>
#include <chrono>

using namespace std;

/* TYPEDEF */

double vmax_scaling = 1.0; //1micromol.min-1.mg of protein-1 = 790mM.min-1

#define pow4(a, b) ((a) * (a) * (a) * (a))
//#define pow4(a, b) std::pow(a,b)
#define pow3(a, b) ((a) * (a) * (a))
#define pow2(a, b) ((a) * (a))
#define Gamma(a,b) GeneGamma[a*GammaInitial.size2()+b]

/*
OPERATION DONE ON THIS FILE
replace dx with r1,r2,r3...
std::pow\(([^n|pow]*),\s*(n\(\d.?,\s*\d.?\))\)  -> pow4($1, $2)

OPERATION **NOT** DONE ON THIS FILE
std::pow\((((?!std::pow|,).)*), 4\) -> pow4($1, 4)
std::pow\(([^,]*), 3\) -> pow3($1, 3)
std::pow\(([^,]*), 2\) -> pow2($1, 2)
*/

void simuPierre::odef_opti(const vector_type &x, vector_type &dxdt, const double /* t */, std::array<double, NUM_OF_PARAMETERS> cell_array, std::vector<double> GeneGamma)
{
	auto &[Gluout, Pi, bx6PG, Ery4P, Lacout, O2, Citrate, Vmf_1, Keq_1, Kgluout_1, Kgluin_1, Vm_2, Ka_2, Kb_2, Keq_2, Kp_2, Kq_2, Vmf_3, Kg6p_3, Vmr_3, Kf6p_3, Kery4p_3, Kfbp_3, Kpg_3, Vm_4, Katp_4, beta_4, alfa_4, Kf26bp_4, Kf6p_4, L_4, Kcit_4, Kiatp_4, Kadp_4, Kfbp_4, Keq_4, Vmf_5, Kfbp_5, Vmr_5, Kdhap_5, Kg3p_5, Kms_6, Kmp_6, Vf_6, Vr_6, Vmf_7, Knad_7, Kg3p_7, Kp_7, Vmr_7, Kdpg_7, Knadh_7, Vmf_8, alfa_8, Ka_8, Kb_8, Vmr_8, beta_8, Kp_8, Kq_8, Kms_9, Kmp_9, Vf_9, Vr_9, Kms_10, Kmp_10, Vf_10, Vr_10, Vmax_11, Kpep_11, Kadp_11, Keq_11, Kpyr_11, Katp_11, Vmf_12, alfa_12, Ka_12, Kb_12, Vmr_12, beta_12, Kp_12, Kq_12, Vm_13, Km_13, k_14, k1_15, k2_15, Vf_16, Vr_16, Kms_16, Kmp_16, Vm_17, Km_17, Vm_18, Km_18, Vm_19, Km_19, Vm_20, Km_20, Vmf_21, Keq_21, Klacin_21, Klacout_21, k1_22, k2_22, k_23, k_24, k_25, k_26, k_27, k_28, Vm_29, Km_29, k_30, O2s, O2gamma, O2n, H_out, pH_eq] = cell_array;
"""

cpp_inter="""
}

void simuPierre::jacobi(const vector_type &x, matrix_type &J, const double &t, vector_type &dfdt, std::array<double, NUM_OF_PARAMETERS> cell_array, std::vector<double> GeneGamma)
{
	auto &[Gluout, Pi, bx6PG, Ery4P, Lacout, O2, Citrate, Vmf_1, Keq_1, Kgluout_1, Kgluin_1, Vm_2, Ka_2, Kb_2, Keq_2, Kp_2, Kq_2, Vmf_3, Kg6p_3, Vmr_3, Kf6p_3, Kery4p_3, Kfbp_3, Kpg_3, Vm_4, Katp_4, beta_4, alfa_4, Kf26bp_4, Kf6p_4, L_4, Kcit_4, Kiatp_4, Kadp_4, Kfbp_4, Keq_4, Vmf_5, Kfbp_5, Vmr_5, Kdhap_5, Kg3p_5, Kms_6, Kmp_6, Vf_6, Vr_6, Vmf_7, Knad_7, Kg3p_7, Kp_7, Vmr_7, Kdpg_7, Knadh_7, Vmf_8, alfa_8, Ka_8, Kb_8, Vmr_8, beta_8, Kp_8, Kq_8, Kms_9, Kmp_9, Vf_9, Vr_9, Kms_10, Kmp_10, Vf_10, Vr_10, Vmax_11, Kpep_11, Kadp_11, Keq_11, Kpyr_11, Katp_11, Vmf_12, alfa_12, Ka_12, Kb_12, Vmr_12, beta_12, Kp_12, Kq_12, Vm_13, Km_13, k_14, k1_15, k2_15, Vf_16, Vr_16, Kms_16, Kmp_16, Vm_17, Km_17, Vm_18, Km_18, Vm_19, Km_19, Vm_20, Km_20, Vmf_21, Keq_21, Klacin_21, Klacout_21, k1_22, k2_22, k_23, k_24, k_25, k_26, k_27, k_28, Vm_29, Km_29, k_30, O2s, O2gamma, O2n, H_out, pH_eq] = cell_array;
	dfdt = boost::numeric::ublas::zero_vector<double>(dfdt.size());
	J = boost::numeric::ublas::zero_matrix<double>(NUM_OF_VARIABLES, NUM_OF_VARIABLES);
"""

cpp_foot="}"


if os.path.exists("ODE_functions.txt"):
    os.remove("ODE_functions.txt")
with open('ODE_functions.txt', 'w') as f:
    print(cpp_header, file=f)

    line="double pH_e" + "=" + cxxcode(pH_e, standard='C++17') + ";"
    print(line, file=f)

    line="double pH_i" + "=" + cxxcode(pH_i, standard='C++17') + ";"
    print(line, file=f)

    line="double pH_inhibition" + "=" + cxxcode(pH_inhibition, standard='C++17') + ";"
    print(line, file=f)

    for i in range(len(flux_symbols)-3):
        line="double r"+ str(i+1) + " = " + cxxcode(flux_symbols[i].subs(pH_inhibition,"pH_inhibition"), standard='C++17') + ";"
        line=re.sub("std::pow\(([^n|pow]*),\s*(n\(\d.?,\s*\d.?\))\)","pow4(\\1, \\2)",line)
        print(line, file=f)
    for i in range(len(Matrix_symp)):
        line="dxdt[" + str(i) + "] = " + cxxcode(Matrix_symp[i].subs(pH_inhibition,"pH_inhibition"), standard='C++17') + ";"
        line=re.sub("std::pow\(([^n|pow]*),\s*(n\(\d.?,\s*\d.?\))\)","pow4(\\1, \\2)",line)
        print(line, file=f)
    print(cpp_inter, file=f)

    for i in range(len(jac_symp.row(0))):
        for j in range(len(jac_symp.col(0))):
            if (cxxcode(sympy.flatten(jac_symp.row(i).col(j))[0], standard='C++17') != "0"):
                line="J(" + str(i) + "," + str(j) + ") = " + cxxcode(sympy.flatten(jac_symp.row(i).col(j))[0].subs(substitution_dict), standard='C++17') + ";"
                line=re.sub("std::pow\(([^n|pow]*),\s*(n\(\d.?,\s*\d.?\))\)","pow4(\\1, \\2)",line)
                print(line, file=f)
    print(cpp_foot, file=f)


