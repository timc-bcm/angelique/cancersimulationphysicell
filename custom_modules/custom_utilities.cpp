#include "custom_utilities.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <boost/tokenizer.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <string>
using namespace boost;
using namespace std;
using std::string;

std::vector<double> vectorSumElemWise(std::vector<double>vec1,std::vector<double>vec2){
	std::transform(vec1.begin(), vec1.end(), vec2.begin(), vec1.begin(), std::plus<double>());
	return vec1;
}


std::vector<double> load_CSV_vec(string file)
{
	std::ifstream data(file);
	std::string line;
	std::vector<double> parsedCsv;
	while (std::getline(data, line))
	{
		if (data.is_open() == false)
		{
			cout << "file: " << file << " not found" << endl;
			_Exit(0);
		}
		std::stringstream lineStream(line);
		std::string cell;
		std::vector<double> parsedRow;
		while (std::getline(lineStream, cell, ','))
		{
			parsedRow.push_back(std::stod(cell));
		}

		parsedCsv.push_back(parsedRow[0]);
	}
	return parsedCsv;
};

std::vector<std::vector<double>> load_CSV_list(string file)
{
	std::ifstream data(file);
	std::string line;
	std::vector<std::vector<double>> parsedCsv;
	while (std::getline(data, line))
	{
		std::stringstream lineStream(line);
		std::string cell;
		std::vector<double> parsedRow;
		while (std::getline(lineStream, cell, ','))
		{
			parsedRow.push_back(std::stod(cell));
		}

		parsedCsv.push_back(parsedRow);
	}
	return parsedCsv;
};


boost::numeric::ublas::matrix<int> load_CSV_mat_int(string file)
{
	std::ifstream data(file);
	std::string line;
	std::vector<std::vector<int>> parsedCsv;
	while (std::getline(data, line))
	{
		std::stringstream lineStream(line);
		std::string cell;
		std::vector<int> parsedRow;
		while (std::getline(lineStream, cell, ','))
		{
			parsedRow.push_back(std::stoi(cell));
		}

		parsedCsv.push_back(parsedRow);
	}
	boost::numeric::ublas::matrix<int> loaded_matrix(parsedCsv.size(), parsedCsv[0].size());
	for (int row = 0; row < parsedCsv.size(); row++)
	{
		for (int col = 0; col < parsedCsv[row].size(); col++)
		{
			loaded_matrix(row, col) = parsedCsv[row][col];
		}
	}
	return loaded_matrix;
};

boost::numeric::ublas::matrix<double> load_CSV_mat(string file)
{
	std::ifstream data(file);
	std::string line;
	std::vector<std::vector<double>> parsedCsv;
	while (std::getline(data, line))
	{
		std::stringstream lineStream(line);
		std::string cell;
		std::vector<double> parsedRow;
		while (std::getline(lineStream, cell, ','))
		{
			parsedRow.push_back(std::stod(cell));
		}

		parsedCsv.push_back(parsedRow);
	}
	boost::numeric::ublas::matrix<double> loaded_matrix(parsedCsv.size(), parsedCsv[0].size());
	for (int row = 0; row < parsedCsv.size(); row++)
	{
		for (int col = 0; col < parsedCsv[row].size(); col++)
		{
			loaded_matrix(row, col) = parsedCsv[row][col];
		}
	}
	return loaded_matrix;
};

std::vector<double> read_cell_var(Cell *pCell, std::vector<string> Variables_names)
{
	std::vector<double> var;
	var.reserve(Variables_names.size());
	for (unsigned int i = 0; i < Variables_names.size(); i++)
	{
		var.push_back(pCell->custom_data[Variables_names[i]]);
	}
	return var;
}
void write_cell_var(Cell *pCell, std::vector<string> Variables_names, std::vector<double> values)
{
	for (unsigned int i = 0; i < Variables_names.size(); i++)
	{
		pCell->custom_data[Variables_names[i]] = values[i];
	}
}

void load_new_metabolites(vector<string> Var_names, vector<double> Init_values)
{
	for (unsigned int i = 0; i < Var_names.size(); i++)
	{
		cell_defaults.custom_data.add_variable(Var_names[i], "dimensionless", Init_values[i]); // mmHg -> mmol/L
	}
}



void cycle_asynchronicity_complete_cycle(Cell *pCell)
{
	int G0_index = pCell->phenotype.cycle.pCycle_Model->find_phase_index("G0");
	int G1_index = pCell->phenotype.cycle.pCycle_Model->find_phase_index("G1");
	int S_index = pCell->phenotype.cycle.pCycle_Model->find_phase_index("S");
	int G2_index = pCell->phenotype.cycle.pCycle_Model->find_phase_index("G2");
	int M_index = pCell->phenotype.cycle.pCycle_Model->find_phase_index("M");

    double G1S = round(1 / (pCell->phenotype.cycle.pCycle_Model->transition_rate(G1_index, S_index) * 60)); //for example 1/(0.00333*60)=5h
    double SG2 = round(1 / (pCell->phenotype.cycle.pCycle_Model->transition_rate(S_index, G2_index) * 60));
    double G2M = round(1 / (pCell->phenotype.cycle.pCycle_Model->transition_rate(G2_index, M_index) * 60));
    double MG1 = round(1 / (pCell->phenotype.cycle.pCycle_Model->transition_rate(M_index, G1_index) * 60));
    //std::cout <<G1S<<" "<<SG2<<" "<<G2M<<" "<<MG1<<" " << std::endl;


    std::vector<int> phases{1,2,3,4};
    std::vector<double> weights {G1S, SG2, G2M, MG1};
    std::discrete_distribution<> dist(weights.begin(), weights.end());
    std::random_device rd;
    std::mt19937 gen(rd());

    int choosen_phase = phases[dist(gen)];
    pCell->phenotype.cycle.data.current_phase_index = choosen_phase;
    return;
}

void create_closely_packed_cell(Cell_Definition &cell_definition)
{
	Cell *pCell = NULL;
	//inspired from https://en.wikipedia.org/wiki/Close-packing_of_equal_spheres

	double x_center = (microenvironment.mesh.bounding_box[0] + microenvironment.mesh.bounding_box[3]) / 2;
	double y_center = (microenvironment.mesh.bounding_box[1] + microenvironment.mesh.bounding_box[4]) / 2;
	double z_center = (microenvironment.mesh.bounding_box[2] + microenvironment.mesh.bounding_box[5]) / 2;

	double c_radius = cell_defaults.phenotype.geometry.radius;
	double radius = parameters.doubles("tumor_radius");

	for (double i = 0; i < 2.0 * radius; i += 1)
	{
		for (double j = 0; j < 2.0 * radius; j += 1)
		{
			for (double k = 0; k <= 2.0 * radius * (1.0 - int(default_microenvironment_options.simulate_2D)); k += 1)
			{
				std::vector<double> cell_position = {(2.0 * (i - radius) + std::fmod((j - radius) + (k - radius), 2.0)) * c_radius, (sqrt(3.0) * ((j - radius) + std::fmod(k - radius, 2.0) / 3.0)) * c_radius, default_microenvironment_options.simulate_2D ? 0.0 : c_radius * (k - radius) * sqrt(6.0) * 2.0 / 3.0};

				double distance = sqrt(pow(cell_position[0] - x_center, 2) + pow(cell_position[1] - y_center, 2) + pow(cell_position[2] - z_center, 2));
				if (distance <= radius * c_radius)
				{
					pCell = create_cell(cell_definition);
					pCell->assign_position(cell_position[0], cell_position[1], cell_position[2]);
					cycle_asynchronicity_complete_cycle(pCell);
				}
			}
		}
	}

	return;
}

void create_border_cells(Cell_Definition &cell_definition){
	Cell *pCell = NULL;
	double border_distance=500;
	double number_of_cells=100;
	double cell_per_pos=2*border_distance/number_of_cells;
	for(double x=-border_distance; x<border_distance;x=x+cell_per_pos){
		pCell = create_cell(cell_definition);
		std::vector<double> cell_position = {x,500,0};
		pCell->assign_position(cell_position);
	}
}



void create_closely_packed_cell_with_density(Cell_Definition &cell_definition)
{
	Cell *pCell = NULL;
	//inspired from https://en.wikipedia.org/wiki/Close-packing_of_equal_spheres

	double x_center = (microenvironment.mesh.bounding_box[0] + microenvironment.mesh.bounding_box[3]) / 2;
	double y_center = (microenvironment.mesh.bounding_box[1] + microenvironment.mesh.bounding_box[4]) / 2;
	double z_center = (microenvironment.mesh.bounding_box[2] + microenvironment.mesh.bounding_box[5]) / 2;

	double radius = cell_defaults.phenotype.geometry.radius;
	double tumor_size = parameters.doubles("tumor_radius");
	std::vector<std::vector<int>> coords={{1,1,1},{1,1,-1},{1,-1,1},{-1,1,1},{1,-1,-1},{-1,-1,1},{-1,1,-1},{-1,-1,-1}};

	for (double combination=0;combination<coords.size();combination++){
	for(double i =0; i< tumor_size; i+=1){
	for(double j =0; j< tumor_size; j+=1){
	for(double k =0; k< ((default_microenvironment_options.simulate_2D == true) ? 1.0 : tumor_size) ; k+=1){
		if(i==0 && j==0 && k==0 && combination!=0){
			break; //if we are at the center of the spheroid don't create 8 cells at the same place.
		}
				double index_dist=sqrt(pow(i - 0, 2) + pow(j - 0, 2) + pow(k - 0, 2));
				//double c_radius=std::min(radius,1+pow(index_dist,2)/50.0);
				double c_radius=radius;
				//TODO: AJUST PRESSURE INSIDE SPHEROID
				int x=i;
				int y=j;
				int z=k;

				std::vector<double> cell_position = {
					(2*x + std::fmod(y+z, 2.0))*c_radius,
					(sqrt(3.0)*(y+ (1/3) * std::fmod(z,2.0)))*c_radius,
					(z*(2*sqrt(6.0))/3.0)*c_radius
					}; 

				double distance = sqrt(pow(cell_position[0] - x_center, 2) + pow(cell_position[1] - y_center, 2) + pow(cell_position[2] - z_center, 2));
				if (distance <= tumor_size * c_radius)
				{
					pCell = create_cell(cell_definition);
					//pCell->phenotype.mechanics.set_absolute_equilibrium_distance(pCell->phenotype,5.0);
					//pCell->phenotype.mechanics.set_relative_maximum_adhesion_distance(15.0);
					//pCell->set_total_volume(pCell->get_total_volume()*(c_radius/radius));
					pCell->assign_position(cell_position[0]*coords[combination][0], cell_position[1]*coords[combination][1], cell_position[2]*coords[combination][2]);
					//pCell->assign_position(0.0,0.00,0.0);
					cycle_asynchronicity_complete_cycle(pCell);
				}



		}
	}
	}
	}
	return;
}

std::vector<std::string> parseCSVLine(std::string line){

   std::vector<std::string> vec;

   // Tokenizes the input string
   tokenizer<escaped_list_separator<char> > tk(line, escaped_list_separator<char>
   ('\\', ';', '\"'));
   for (auto i = tk.begin();  i!=tk.end();  ++i)
   vec.push_back(*i);

   return vec;
}

void load_cells_from_file(Cell_Definition &cell_definition, std::string path){
	Cell *pCell = NULL;

std::ifstream file(path);
if (file.is_open()) {
    std::string line;
	int line_num=0;
    while (std::getline(file, line)) {
        // using printf() in all tests for consistency
		if(line_num!=0){

		auto csv_values = parseCSVLine(line);
		pCell = create_cell(cell_definition);
		double cell_ID=stod(csv_values[0]);
		double x=stod(csv_values[1]);
		double y=stod(csv_values[2]);
		double z=stod(csv_values[3]);
		double cell_volume=stod(csv_values[4]);

		pCell->assign_position({x,y,z});
		pCell->set_total_volume(cell_volume);
		pCell->ID=cell_ID;
		cycle_asynchronicity_complete_cycle(pCell);
		}
		line_num++;
    }
    file.close();
}
}


void swap (int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
  
// A function to generate a random
// permutation of arr[]
std::vector<int> randomize_fisher_yates(std::vector<int> arr, int n)
{
    // Use a different seed value so that
    // we don't get same result each time
    // we run this program
    srand (time(NULL));
 
    // Start from the last element and swap
    // one by one. We don't need to run for
    // the first element that's why i > 0
    for (int i = n - 1; i > 0; i--)
    {
        // Pick a random index from 0 to i
        int j = rand() % (i + 1);
 
        // Swap arr[i] with the element
        // at random index
        swap(&arr[i], &arr[j]);
    }
	return arr;
}
 













/*
void create_closely_packed_cell_with_density(Cell_Definition &cell_definition)
{
	Cell *pCell = NULL;
	//inspired from https://en.wikipedia.org/wiki/Close-packing_of_equal_spheres

	double x_center = (microenvironment.mesh.bounding_box[0] + microenvironment.mesh.bounding_box[3]) / 2;
	double y_center = (microenvironment.mesh.bounding_box[1] + microenvironment.mesh.bounding_box[4]) / 2;
	double z_center = (microenvironment.mesh.bounding_box[2] + microenvironment.mesh.bounding_box[5]) / 2;

	double radius = cell_defaults.phenotype.geometry.radius;
	double tumor_size = parameters.doubles("tumor_radius");
	std::vector<std::vector<int>> coords={{1,1,1},{1,1,-1},{1,-1,1},{-1,1,1},{1,-1,-1},{-1,-1,1},{-1,1,-1},{-1,-1,-1}};

	for (double combination=0;combination<coords.size();combination++){
	for(double i =0; i< tumor_size; i+=1){
	for(double j =0; j< tumor_size; j+=1){
	for(double k =0; k< default_microenvironment_options.simulate_2D == true ? tumor_size:0 ; k+=1){
				double index_dist=sqrt(pow(i - 0, 2) + pow(j - 0, 2) + pow(k - 0, 2));
				double c_radius=std::min(radius,1+pow(index_dist,2)/50.0);
				//double c_radius=radius;
				//TODO: AJUST PRESSURE INSIDE SPHEROID
				int x=i;
				int y=j;
				int z=k;

				std::vector<double> cell_position = {
					(2*x + std::fmod(y+z, 2.0))*c_radius,
					(sqrt(3.0)*(y+ (1/3) * std::fmod(z,2.0)))*c_radius,
					(z*(2*sqrt(6.0))/3.0)*c_radius
					}; 

				double distance = sqrt(pow(cell_position[0] - x_center, 2) + pow(cell_position[1] - y_center, 2) + pow(cell_position[2] - z_center, 2));
				if (distance <= tumor_size * c_radius)
				{
					pCell = create_cell(cell_definition);
					//pCell->phenotype.mechanics.set_absolute_equilibrium_distance(pCell->phenotype,5.0);
					//pCell->phenotype.mechanics.set_relative_maximum_adhesion_distance(15.0);
					//pCell->set_total_volume(pCell->get_total_volume()*(c_radius/radius));
					double decrease=0.0;//pow(0.01*distance,2);
					pCell->assign_position(cell_position[0]*coords[combination][0], cell_position[1]*coords[combination][1], cell_position[2]*coords[combination][2]);
					cycle_asynchronicity_complete_cycle(pCell);
				}



		}
	}
	}
	}
	return;
}
*/