/*

*/

#include "../modules/PhysiCell_settings.h"
#include <algorithm>
#include <iostream>
#include <fstream>
#include <chrono>
#include "heterogeneous_diffusion.h"
/* TYPEDEF */
typedef std::chrono::high_resolution_clock Clock;

using namespace std;
double kernel_gaussian[5];
std::vector<std::vector<Cell *>> diffusion_and_cells_map;

void calc_gaussian_kernel_1D(double kernel_gaussian[5])
{
	double sigma = 0.8;
	double q = 2.0 * sigma * sigma;
	double sum = 0.0;
	for (int x = -2; x <= 2; x++)
	{
		kernel_gaussian[x + 2] = (exp(-(x * x) / q)) / (sqrt(2 * M_PI) * sigma);
		sum += kernel_gaussian[x + 2];
	}
	for (int i = 0; i < 5; i++)
		kernel_gaussian[i] /= sum;
}

int clamp(int x, int pad, int size)
{
	if (x + pad < 0 || x + pad >= size)
	{
		return x - pad;
	}
	else
	{
		return x + pad;
	}
}

void empty_substrate_at_borders(int substrate, bool &border_setup, std::vector<int>& borders)
{
	if (border_setup == false)
	{
		for (int n = 0; n < microenvironment.number_of_voxels(); n++)
		{
			if (microenvironment.voxels(n).is_Dirichlet == true)
			{
				borders.push_back(n);
				//std::cout<<borders.back()<<std::endl;
			}
		}
		border_setup = true;
	}

#pragma omp parallel for
	for (int v = 0; v < borders.size(); v++)
	{
		microenvironment(borders.at(v))[substrate] = 0.0;
	}
}

void update_diffusion_map(double dt)
{
	//auto t1 = Clock::now();

	microenvironment.diffusion_coeff_map = microenvironment.diffusion_coeff_map_tzero;			  //TIMC
	microenvironment.density_map = std::vector<double>(microenvironment.number_of_voxels(), 0.0); //TIMC
	diffusion_and_cells_map.clear();
	diffusion_and_cells_map.resize(microenvironment.number_of_voxels(), std::vector<Cell *>(0)); //TIMC
	double sample_radius = 15 + 30;
	//micrometer = two and a half mechanical voxel
	/*
	- itérer sur les voxels mécaniques
	- récupérer les cellules à l'intérieur ainsi que dans le voisinage moore
	- calculer la densité moyenne
	- assigner la diffusion par rapport à la densité sur chacun des voxels du microenvironnement associées aux cellules contenues dans le voxel mécanique
	*/
	PhysiCell::Cell_Container *cell_container = (*all_cells)[0]->get_container();
	std::vector<std::vector<Cell *>> mechanical_grid = (*all_cells)[0]->get_container()->agent_grid;
	for (int i = 0; i < mechanical_grid.size(); i++)
	{
		if (mechanical_grid[i].size() > 0)
		{

			///////////////////// GET MICROENV VOXEL (diffusive)
			std::vector<int> microenv_voxels_inside_mechanics = {};
			for (std::vector<PhysiCell::Cell *>::iterator pcell = mechanical_grid[i].begin(); pcell != mechanical_grid[i].end(); pcell++)
			{
				int voxel_of_cell = (*pcell)->get_current_voxel_index();
				if (std::find(microenv_voxels_inside_mechanics.begin(), microenv_voxels_inside_mechanics.end(), voxel_of_cell) != microenv_voxels_inside_mechanics.end())
				{
					continue;
				}
				else
				{
					microenv_voxels_inside_mechanics.push_back(voxel_of_cell);
					diffusion_and_cells_map[voxel_of_cell].push_back(*pcell);
				}
			}

			///////////////////// GET NEIGHBORS
			std::vector<int> moore_neighborhood_0 = cell_container->underlying_mesh.moore_connected_voxel_indices[i]; //get voxel neighborhood ids
			moore_neighborhood_0.push_back(i);																		  //add the center voxel to the list of neighbors //8 -> 9 voxels to make a full square.
			std::vector<int> moore_neighborhood = {};
			for (int neighbor0 = 0; neighbor0 < moore_neighborhood_0.size(); neighbor0++)
			{
				moore_neighborhood.push_back(moore_neighborhood_0[neighbor0]);
				std::vector<int> neighbors_of_neighbors = cell_container->underlying_mesh.moore_connected_voxel_indices[moore_neighborhood_0[neighbor0]];
				moore_neighborhood.insert(moore_neighborhood.end(), neighbors_of_neighbors.begin(), neighbors_of_neighbors.end());
			}

			//remove duplicate voxels in neighborhood if any.
			sort(moore_neighborhood.begin(), moore_neighborhood.end());
			moore_neighborhood.erase(unique(moore_neighborhood.begin(), moore_neighborhood.end()), moore_neighborhood.end());

			std::vector<PhysiCell::Cell *> neighborhood_cells;

			//iterate on neighborvoxels
			for (std::vector<int>::iterator it = moore_neighborhood.begin(); it != moore_neighborhood.end(); ++it)
			{
				for (std::vector<PhysiCell::Cell *>::iterator pcell = mechanical_grid[*it].begin(); pcell != mechanical_grid[*it].end(); pcell++)
				{
					neighborhood_cells.push_back(*pcell);
				}
			}

			///////////////////// CALCULATE VOLUME OCCUPATION
			for (int microenv_voxel = 0; microenv_voxel < microenv_voxels_inside_mechanics.size(); microenv_voxel++)
			{
				int microenv_voxel_id = microenv_voxels_inside_mechanics[microenv_voxel];
				int cell_count = 0;
				double volume_occupation = 0.0;
				//std::vector<double> coords_of_mecha_vox_center = cell_container->underlying_mesh.voxels[i].center;
				std::vector<double> coords_of_vox_center = microenvironment.voxels(microenv_voxel_id).center;

#pragma omp parallel for reduction(+ \
								   : volume_occupation, cell_count)
				for (int cell_num = 0; cell_num < neighborhood_cells.size(); cell_num++)
				{
					PhysiCell::Cell *pcell = neighborhood_cells[cell_num];
					//iterate on neighbors cells and get volume
					double distance_from_voxel_center = sqrt(pow(pcell->position[0] - coords_of_vox_center[0], 2) + pow(pcell->position[1] - coords_of_vox_center[1], 2) + pow(pcell->position[2] - coords_of_vox_center[2], 2));
					//check if cell is in radius or slightly "out" (if so the entire volume of this cell can't be counted)

					double radius_of_cell = pcell->phenotype.geometry.radius;
					if (distance_from_voxel_center + radius_of_cell < sample_radius)
					{
						cell_count += 1;
						volume_occupation += (default_microenvironment_options.simulate_2D == false) ? pcell->phenotype.volume.total : M_PI * pow(pcell->phenotype.geometry.radius, 2);
					}
					else
					{
						if (distance_from_voxel_center - radius_of_cell < sample_radius)
						{ //if cell is out of the sampled radius near the border
							cell_count += 1;
							if (default_microenvironment_options.simulate_2D == false)
							{
								//volume of the intersection (length) of two overlapping sphere: http://intranet-statique.enstimac.fr/crcmath/math/math/s/s563.htm
								volume_occupation += M_PI * pow(sample_radius + radius_of_cell - distance_from_voxel_center, 2.0) * (pow(distance_from_voxel_center, 2) + 2 * distance_from_voxel_center * radius_of_cell - 3 * pow(radius_of_cell, 2) + 2 * distance_from_voxel_center * sample_radius + 6 * radius_of_cell * sample_radius - 3 * pow(sample_radius, 2)) / (12 * distance_from_voxel_center);
							}
							else
							{
								//area of the intersection of two overlapping circle: https://mathworld.wolfram.com/Circle-CircleIntersection.html
								volume_occupation += pow(radius_of_cell, 2) * acos((pow(distance_from_voxel_center, 2) + pow(radius_of_cell, 2) - pow(sample_radius, 2)) / (2 * distance_from_voxel_center * radius_of_cell)) + pow(sample_radius, 2) * acos((pow(distance_from_voxel_center, 2) + pow(sample_radius, 2) - pow(radius_of_cell, 2)) / (2 * distance_from_voxel_center * sample_radius)) - 0.5 * sqrt((-distance_from_voxel_center + radius_of_cell + sample_radius) * (distance_from_voxel_center + radius_of_cell - sample_radius) * (distance_from_voxel_center - radius_of_cell + sample_radius) * (distance_from_voxel_center + radius_of_cell + sample_radius));
							}
						}
						else
						{
							volume_occupation += 0.0;
						}
					}
				}

				if (default_microenvironment_options.simulate_2D == false)
				{
					volume_occupation = volume_occupation / (4 * M_PI * pow(sample_radius, 3.0) / 3.0); //sample volume
				}
				else
				{
					volume_occupation = volume_occupation / (M_PI * pow(sample_radius, 2.0)); //sample area
				}

				microenvironment.density_map[microenv_voxel_id] = volume_occupation;
				for (unsigned substrat = 0; substrat < microenvironment.number_of_densities(); substrat++)
				{
					microenvironment.diffusion_coeff_map[substrat][microenv_voxel_id] = max(0.01 * microenvironment.diffusion_coeff_map_tzero[substrat][microenv_voxel_id], microenvironment.diffusion_coeff_map_tzero[substrat][microenv_voxel_id] * (1.0 - 0.25 * volume_occupation));
				}
			}
		}
	}

	if (parameters.bools("smooth_density") == true)
	{
		for (unsigned substrat = 0; substrat < microenvironment.number_of_densities(); substrat++)
		{
			//Smooth the diffusion coefficients to avoid any instabilities
			int center_kernel = 2; // for a kernel of size 5
			int maxX = microenvironment.mesh.x_coordinates.size();
			int maxY = microenvironment.mesh.y_coordinates.size();
			int maxZ = microenvironment.mesh.z_coordinates.size();

			std::vector<double> temp_vector = microenvironment.diffusion_coeff_map[substrat];
			std::vector<double>(microenvironment.diffusion_coeff_map[substrat].size(), 0.0);

#pragma omp parallel for
			for (unsigned int vox = 0; vox < microenvironment.number_of_voxels(); vox++)
			{
				int x = microenvironment.cartesian_indices(vox)[0];
				int y = microenvironment.cartesian_indices(vox)[1];
				int z = microenvironment.cartesian_indices(vox)[2];
				microenvironment.diffusion_coeff_map[substrat][vox] = temp_vector[microenvironment.voxel_index(clamp(x, -2, maxX), y, z)] * kernel_gaussian[0] + temp_vector[microenvironment.voxel_index(clamp(x, -1, maxX), y, z)] * kernel_gaussian[1] + temp_vector[vox] * kernel_gaussian[2] + temp_vector[microenvironment.voxel_index(clamp(x, 1, maxX), y, z)] * kernel_gaussian[3] + temp_vector[microenvironment.voxel_index(clamp(x, 2, maxX), y, z)] * kernel_gaussian[4];
			}

			temp_vector = microenvironment.diffusion_coeff_map[substrat];

#pragma omp parallel for
			for (unsigned int vox = 0; vox < microenvironment.number_of_voxels(); vox++)
			{
				int x = microenvironment.cartesian_indices(vox)[0];
				int y = microenvironment.cartesian_indices(vox)[1];
				int z = microenvironment.cartesian_indices(vox)[2];
				microenvironment.diffusion_coeff_map[substrat][vox] = temp_vector[microenvironment.voxel_index(x, clamp(y, -2, maxY), z)] * kernel_gaussian[0] + temp_vector[microenvironment.voxel_index(x, clamp(y, -1, maxY), z)] * kernel_gaussian[1] + temp_vector[vox] * kernel_gaussian[2] + temp_vector[microenvironment.voxel_index(x, clamp(y, 1, maxY), z)] * kernel_gaussian[3] + temp_vector[microenvironment.voxel_index(x, clamp(y, 2, maxY), z)] * kernel_gaussian[4];
			}

			temp_vector = microenvironment.diffusion_coeff_map[substrat];

			if (default_microenvironment_options.simulate_2D == false)
			{
#pragma omp parallel for
				for (unsigned int vox = 0; vox < microenvironment.number_of_voxels(); vox++)
				{
					int x = microenvironment.cartesian_indices(vox)[0];
					int y = microenvironment.cartesian_indices(vox)[1];
					int z = microenvironment.cartesian_indices(vox)[2];
					microenvironment.diffusion_coeff_map[substrat][vox] = temp_vector[microenvironment.voxel_index(x, y, clamp(z, -2, maxZ))] * kernel_gaussian[0] + temp_vector[microenvironment.voxel_index(x, y, clamp(z, -1, maxZ))] * kernel_gaussian[1] + temp_vector[vox] * kernel_gaussian[2] + temp_vector[microenvironment.voxel_index(x, y, clamp(z, 1, maxZ))] * kernel_gaussian[3] + temp_vector[microenvironment.voxel_index(x, y, clamp(z, 2, maxZ))] * kernel_gaussian[4];
				}
			}
		}
	}
}
