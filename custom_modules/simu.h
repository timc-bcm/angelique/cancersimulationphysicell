#ifndef SIMU_H_INCLUDED
#define SIMU_H_INCLUDED


#include "../core/PhysiCell.h"
#include "../modules/PhysiCell_standard_modules.h"
#include "heterogeneous_diffusion.h"
#include "./custom_utilities.h"

#include <boost/numeric/odeint.hpp>
#include <boost/algorithm/algorithm.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
using namespace BioFVM;
using namespace PhysiCell;

#define NUM_OF_VARIABLES 85
#define NUM_OF_PARAMETERS 119
/* TYPEDEF */
typedef boost::numeric::ublas::vector<double> state_T;
typedef boost::numeric::ublas::vector<double> vector_type;
typedef boost::numeric::ublas::matrix<double> matrix_type;

class Simu
{

public:
    virtual void update_phenotype(Cell *pCell, Phenotype &phenotype, double dt)=0;
    virtual void ODE_Cells(double dt)=0;
    virtual void init_cell_definitions()=0;
    virtual void load_metabolites_values()=0;
    virtual void FluxToMicroenv(double number_of_steps)=0;
    std::vector<std::vector<double>> microenv_substrat_dept;
    bool mechanics_updated_one_time=false;
    bool phenotype_updated_one_time=false;
    double abstol_solve;
    double reltol_solve;
    //bool dirichlet_borders_set;
    //std::vector<int> dirichlet_borders;
};

class simuPierre : public Simu
{
public:
    void update_phenotype(Cell *pCell, Phenotype &phenotype, double dt);
    void ODE_Cells(double dt);
    void init_cell_definitions();
    void odef_opti(const vector_type &x, vector_type &dxdt, const double /* t */, std::array<double, 119> cell_array, std::vector<double> GeneGamma);
    void jacobi(const vector_type &x, matrix_type &J, const double &t, vector_type &dfdt, std::array<double, 119> cell_array,std::vector<double> GeneGamma);
    void load_metabolites_values();
    void FluxToMicroenv(double number_of_steps);
    std::vector<double> A;
    std::vector<double> D;
    std::vector<double> lx;
    boost::numeric::ublas::matrix<double> GammaInitial;
    boost::numeric::ublas::matrix<int> n;
    boost::numeric::ublas::matrix<double> S;
    struct ode;
};

class simuKevin : public Simu
{
public:
    void update_phenotype(Cell *pCell, Phenotype &phenotype, double dt);
    void ODE_Cells(double dt);
    void init_cell_definitions();
    void load_metabolites_values();
    void FluxToMicroenv(double number_of_steps);
    struct ode;
};

Simu* get_simu_type();
#endif